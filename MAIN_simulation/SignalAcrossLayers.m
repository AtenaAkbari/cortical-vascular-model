%%
% BOLDACROSSLAYERS
%
% Author: Irati Markuerkiaga
% VASO addition: Atena Akbari
%
% Description: Calculates the BOLD and VASO signal change upon activation for every
% layer.
%
% OUTPUT:
%   - fields in layersOut:
%   *BOLD_layer: BOLD signal across all layers for SE and GE. Size per
%   default: 2 x numberOfLayers x numberOfB0% *S_R2compartment_Layer:
%   Intravascular and Extravascular R2 values for all compartments at
%   baseline *S_R2compartment_act_Layer: Intravascular and Extravascular
%   R2 values for all compartments upon activation * S_BOLD_std_out: std of
%   BOLD signal when noise is simulated on top of it

% ASSUMPTIONS/SIMPLIFICATIONS
%
% NOTE: By switching the simulation mode this file can: 
% 1- Predict the BOLD signal across layers in a standard way
% 2- Calculate the outcome for different TEs at 7T and compare it to the 
% results in Poser 2009(NeuroImage 45 (2009) 1162�1172) 
% 3- Check how the BOLD signal changes with the underlying neural
% activation

%%
function [layersOut, NVC, actChange] = SignalAcrossLayers(VM, NVC, MR, VMout, VR, modality)

% INITIALIZE VARIABLES AND CONSTANTS
layersOut = struct;
% PREDICT THE BOLD SIGNAL FOR EACH LAYER
% 'standard' mode; Uludag (2009)
if (strcmp(modality, 'VASO')) 
    for layerIdx = 1 : VM.nLayers + VM.simuExtraCortVein
    [layersOut(layerIdx).data, NVC, actChange] = UludagLayers(VM, NVC, MR, VMout,VR, layerIdx, 'VASO');
    end
    
elseif (strcmp(modality, 'BOLD')) 
    for layerIdx = 1 : VM.nLayers + VM.simuExtraCortVein
        [layersOut(layerIdx).data, NVC, actChange] = UludagLayers(VM, NVC, MR, VMout,VR, layerIdx, 'BOLD');
    end
    
%% Didplay message in command window
fprintf('%s\n%s\n%s\n', VR.separatingAsterisks,'Signal across layers computed',VR.separatingAsterisks)
fprintf('Simulation mode: %s\n', MR.simuMode)
end
