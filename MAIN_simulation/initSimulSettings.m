%%
% INITSIMULSETTINGS
%
% Author: Irati Markuerkiaga, 22.08.2014
% VASO addition: Atena Akbari
%
% Description: Initialises all global parameters required for the simulation
%
% There are five main structs, which refer to different types of
% parameters:
% - VM: Vascular Model
% - NVC: Neurovascular Coupling
% - MR: Magnetic Resonance
% - VR: Visualization of Results
% - VMout: Vascular Model Output  
%
% NOTE: 
% - If MR.IV_R2_mode == 'bloodR2revised', then field strengths simulated are
% 3T and 7T. On 2014.11.06 it can only be run together with Uludag2009 and
% full activation.
% - Unless otherwise stated, units are [mm] or [s]
% Change the step size in "NVC.cbv_values = (0.001:0.05:0.6)" from 0.05 to
% 0.01 to produce the paper figures. 

%%
% LOAD PATHS
addpath(fullfile (pwd, 'SignalModel_Uludag'))
addpath(fullfile (pwd, 'BuildVascularModel'))
addpath(fullfile (pwd, 'PlotResults'))
addpath(fullfile (pwd, 'Export_fig'))


%%
% INITIALIZE VARIABLES
VM    = struct();                                                                    % VM: Vascular Model
NVC   = struct();                                                                    % NVC: Neurovascular Coupling
MR    = struct();                                                                    % MR: Magnetic Resonance
VR    = struct();                                                                    % VR: Visualization
VMout = struct();                                                                    % VMout: Output of the Vascular Model

%*************************************************************************
%% Vascular Model (VM)
%*************************************************************************
%%
% PARAMETERS USED FOR BUILDING THE MODEL
% CONSTANTS 
VM.lp.multiFactor = 1;
VM.lp.corticalThickness    = 2.5;                                                   % [mm] cortLayersNames = ['II/III'; 'IVA   ';'IVB   ';'IVCa  ';'IVCb  ';'V     ';'VI    '];
VM.lp.voxelSide            = 0.75;                                                  % [mm] length of the side of the isotropic voxel simulated
VM.lp.voxelLength          = 0.25;                                                  % [mm] height of the voxel simulated
VM.lp.voxelVol             = VM.lp.voxelSide ^2 * VM.lp.voxelLength;                % [mm3] volume of voxel simulated
VM.lp.voxLayer             = [2 1 4 2 1];                                           % Number of voxels per layers (i.e., 2 voxels in layer VI, 1 voxel in layer V, 4 voxels in layer IV, 2 voxels in layers II/III, 1 in layer I ) (de Sousa 2010)
VM.lp.voxLayLim            = cumsum(VM.lp.voxLayer);
VM.lp.cortLayersBoundaries = [1 30; 2 7; 3 15; 4 10; 5 10; 6 10; 7 18];             % Thicknesses of the cortical layers as defined in cortLayersNames taken from de Sousa 2010, CSF -> WM
% VM.lp.alpha_cap            = (2.2/8)*1000;                                        % Regression factor obtained from Lipowski2005
% VM.lp.alpha_pre_cap        = (3.9/9.5)*1000;
% VM.lp.alpha_post_cap       = (2.2/9.5)*1000;
VM.lp.alpha_cap            = (1.6/8)*1000;                                          % Regression factor obtained from Lipowski2005
VM.lp.alpha_pre_cap        = (4/12)*1000;
VM.lp.alpha_post_cap       = (2/12)*1000;
VM.lp.meshFraction_orig    = [0.02 0.0215 0.022 0.0255 0.027 0.021 0.024];          % baseline blood volume in the laminar netwrok. [III/II/I IVa IVb IVc-a IVc-b V VI] taken from V1 of Macaque in Weber 2008. Correspond to
                                                                                    % the "depth-dependent" baseline CBV in the paper. 
VM.lp.meshFraction_orig    = [0.0230 0.0230 0.0230 0.0230 0.0230 0.0230 0.0230];    % baseline blood volume in the laminar netwrok correspond to the "constant baseline CBV" scenario in the paper.  
VM.lp.newVessLayer_vein    = 2*VM.lp.multiFactor;                                                     % Number of new vessels per vascular layer (Park 2005, Duvernoy 1981)
VM.lp.newVessLayer_art     = 4*VM.lp.multiFactor;                                                     % maybe four times higher than veins 
VM.lp.nrBigVes_vein        = 1*VM.lp.multiFactor;
VM.lp.nrBigVes_art         = 2*VM.lp.multiFactor;                                                     % Number of big vessels (G4 or G5) that are present in all  layers
VM.lp.nICV_array           = [4 3 3 2 2 2 2 1 1 1];                                                   % this is type of vessels, like 4 types of ICV   
VM.lp.nICA_array           = [4 3 3 2 2 2 2 1 1 1];  
VM.lp.numVesselLayer_vein  = [6 4 4 2 2 2 2 1 1 1]*VM.lp.multiFactor;                                 % Number of ICV vessels in each of the 10 voxels, based on vascLayersBoundaries and voxelLenght, if these change, this parameter will have to vary 
VM.lp.numVesselLayer_art   = [12 8 8 4 4 4 4 2 2 2]*VM.lp.multiFactor;  
VM.lp.idxICAtype           = [2 2 4 4]*VM.lp.multiFactor;                                             % Nr of ICVs of each kind (i.e. [G4 G3 G2 G1]  
VM.lp.idxICVtype           = [1 1 2 2]*VM.lp.multiFactor;
     
% PARAMETERS USED IN VARIOUS FUNCTIONS
VM.nMicroVascComp          = 5;
VM.nLayers                 = 10;
VM.nICV                    = 4;
VM.nICA                    = 4;
VM.propagation             = 0;

% SIMULATE PIAL VEINS
VM.simuExtraCortVein       = 0;                                                     % Simulate extracortical vein, '1': yes, '0': no
% VM.PialOrientation         = 'perpendicular';                                     % Orientation of the pial vein
VM.PialOrientation         = 'random';                                              % Orientation of the pial vein

%*************************************************************************
%% Neurovascular Coupling (NVC)
%*************************************************************************
% MICROVASCULAR DILATION MODEL, see in the end of the file
NVC.dilationMode          = 'Uludag2009'; 
NVC.hiperoxia             = false(1);                                                % Simulation of hyperoxic state: no dilation, increased baseline and activated vessel oxygenations
% ACTIVATION STRENGTH THROUGHOUT THE LAYERS
NVC.relStrength           = [1 1 1 1.5 1.5 1.5 1.5 1 1 1];                           % Relative strength of activation in the laminar netwrok correspond to the "higher activation strength" in middle cortical depths. 
                                                                                     % '1'-> maximum activation, '0' -> no activation. To simulate the "equal activation strength" across the layers, chnage all digits to '1'. 
NVC.relStrength_ica       = [1 1 1 1.5 1.5 1.5 1.5 1.5 1.5 1.5];                     % Relative strength of activation in ICAs and ICVs correspond to "higher activation strength in middle and upper cortical depth".
                                                                                     % To simulate the "equal activation strength across the layers" change all digits to '1'. 
if NVC.hiperoxia
    NVC.oxygBas_cte       = 0.7;                                                     % AURKITU ZER BALIO IZANGO LIRATEKEEN ZENTZUDUNAK HEMEN!!!, uste dut badaukadala, begiratu Oxford-eko kodea
    NVC.oxygAct_cte       = 0.8;
    NVC.isHiperoxia       = 0;                                                       % isHyperoxia is an inverted flag, if = '0' -> hyperoxic state simulated
else
    NVC.oxygBas_cte       = 0.60;
    NVC.oxygAct_cte       = 0.75;
    NVC.isHiperoxia       = 1;
end

% Maximum CBF and CMRO2 increase upon activation
NVC.deltaCBFmax           = 0.50;                                                     % 0.50 in Uludag2009
NVC.deltaCMRO2max         = 0.286;                                                    % REF!
NVC.tau                   = 2.45;                                                     % Constant for the sigmoid relating activation strength and CBF
NVC.n                     = 4;                                                        % 4: derived from Uludag2009, Buxton's latest pubs give values between 2 and 3

% Oxygen saturation
NVC.x_bas                 = 0.95;                                                     % Arteriole at baseline
NVC.x_act                 = 1;                                                        % Arteriole upon activation
NVC.oxygBas_cte           = 0.6;                                                      % Venule at baseline
NVC.veinFullDeltaY        = 0.1;                                                      % Oxygenation difference between baseline and full activation in a vein

% Proton density
NVC.Rho_GM                = 1;
NVC.Rho_bl                = 1;
NVC.Phi                   = 1;                                                        % IV/EV spin density ratio. Slightly higher than 1 in general, but can be decreased if diffusion weighted grads used or increased by inflow of unsaturated fresh blood.
NVC.d_ex                  = 1;                                                        % EV compression factor. Value = 1 implies that after vessel expansion, the amount of protons in the voxel remains the same. Meaning that either it is displaced outside the voxel or goes into IV space. This makes sense because water is incompressible.

% Blood related parameters
NVC.deltaChi_0            = 4*pi*0.264;                                               % Susceptibility of blood with fully deoxygenated blood [ppm]
NVC.Hct                   = 0.4;                                                      % Hematocrit
NVC.Yoff                  = 0.95;                                                     % Reported betw 0.8-1.0, oxygen saturation that results in no difference between blood and surrounding tissue

% Microvascular volume fractions, following Boas2008
NVC.arteriolFrac_micro   = 0.12*2;
NVC.capFrac              = 0.36;
NVC.venuleFrac_micro     = 0.20/2;                              
NVC.arteriolFrac_macro   = 0.10*2;
NVC.venuleFrac_macro     = 0.20/2;

% Vessel sizes used in the model
NVC.diam_cap            = 8/10^3;                                                      % Diameter of a capillary [mm]
NVC.l_cap               = 250/10^3;                                                    % Length of a capillary [mm]
% NVC.diam_pial           = 200/10^3;                                                  % Diameter of an average pial vein [mm]
NVC.diam_pial           = 250/10^3;                                                    % Pial vein diameter in order to have the big large contrib observed in Koopmans2011
% NVC.pial_CBV           = pi*(NVC.diam_pial/2)^2/(VM.lp.voxelSide ^2);
NVC.pial_CBV            = pi*(NVC.diam_pial/2)^2/(VM.lp.voxelSide*VM.lp.voxelLength);
NVC.pial_d_CBV          = 0;

NVC.cbv_values = (0.001:0.05:0.6);                                                   % delta CBV range selected for simulation 
NVC.CBV_cap = NVC.cbv_values;                                                        % capillaries deltaCBV
NVC.CBV_art = NVC.cbv_values;                                                        % arterioles deltaCBV
NVC.CBV_ven = NVC.cbv_values;                                                        % venules delta CBV
NVC.CBV_ICA = NVC.cbv_values;                                                        % ICAs and ICVs delta CBV
NVC.delta_CBV_range = 1:length(NVC.cbv_values);

NVC.ven_oxy_base =  0.6:.01:0.75;                                                    % venules and ICVs oxygen saturation at baseline 
NVC.ven_oxy_act =  0.75:.01:0.9;                                                     % venules and ICVs oxygen saturation at activation 
%*************************************************************************
%% Magnetic Resonance (MR)
%*************************************************************************
MR.gamma                = 2*pi*42.6;                                                    % Gyromagnetic ratio of 1H [MHz/T]

% Field and TE parameters
MR.TE_SE_all            = [0.096 0.077 0.065 0.060 0.050 0.041 0.033 0.029 0.026];      % TE for SE from the Uludag paper , Write echo time used by Koopmans at 3T and 7T
MR.TE_GE_all            = [0.066 0.048 0.041 0.037 0.028 0.022 0.019 0.017 0.015];      % TE for GE from the Uludag paper , Write echo time used by Koopmans at 3T and 7T
MR.B0_all               = [1.5 3 4 4.7 7 9.4 11.7 14 16.1];

% SELECT SIMULATION MODE
MR.simuMode             = 'standard';
% MR.simuMode           = 'multiTE';

% SELECT RELAXATION RATES
MR.KrishnaT2in          = [60.3, 77.2; 20.5, 29.7];                                     % Intravascular T2 times as given in Krishnamurthy 2014. T2[normOx@3T, hyperOx@3T; normOx@7T, hyperOx@7T] Actually they compare normoxia and hyperoxia, but the Y they report for thyperoxia is almost 70%, the value Uludag gives for the active state, and normoxia around 60%, the baseline state in Uludag2009. "Dependence of Blood T2 on Oxygenation at 7 T: In Vitro Calibration and In Vivo Application"
MR.BlockleyT2starin     = [14, 20; 4.2, 5.9];                                           % Intravascluar T2star times as given in Blockley 2008. T2star [Y=60@3T, Y=70@3T; Y=60@7T, Y=70@7T]. "Field Strength Dependence of R1 and R* 2 Relaxivities of Human Whole Blood to ProHance, Vasovist, and Deoxyhemoglobin"
% MR.EV_R2_mode          = 'LayerR2star';                                               % Koopmans T2* measurements across the cortex both for 3T and 7T
MR.EV_R2_mode           = 'cortexR2star';                                               % Extravascular T2* values taken from Uludag2009
MR.IV_R2_mode           = 'bloodR2revised';                                             % Blood R2 values are taken from Krishnamurthy2014/Gardener 2010 or extrapolated from Budde2013
% MR.IV_R2_mode          = 'bloodR2standard';                                           % Blood R2 values are taken from Uludag2009, no IV contrib at 7T
% MR.IV_R2_mode          = 'IV_R2starRevised';                                          % Due to the small size of the vessel, the magnetic inhomogeneities in them can be neglected and therefore there is no T2' contribution
MR.T2blood_starAtHF     = 0;

if strcmp(MR.IV_R2_mode, 'IV_R2starRevised')
%     R2star_adjustmentFactor = 2;                                                     % GE @7T: 18% IV @TE=T2_[GM]
%     R2star_adjustmentFactor = 3;                                                     % GE @7T: 12% IV @TE=T2_[GM]
    R2star_adjustmentFactor = 4;                                                       % GE, @7T: 8% IV @TE=T2_[GM]
end

% SELECT FIELD STRENGTHS TO SIMULATE
MR.simuField           = logical([0 0 0 0 1 0 0 0 0]);                                 % Mask to choose which field strengths the user wants to simulate. [1.5 3 4 4.7 7 9.4 11.7 14 16.1]
MR.seq                 = ['SE';'GE'];
MR.multiEcho_7T        = [4.8 10.5 16.2 21.9 27.6 33.3 39 44.7 50.4 56.1]/1000;        % Echoes from Koopmans2011, Figure 5.5
MR.selectEchoes        = 1:length(MR.multiEcho_7T);                                    % Select the echoes you want to plot
 
% APPLY MASK
MR.B0                  = MR.B0_all(MR.simuField);                                               
MR.TE_SE               = MR.TE_SE_all(MR.simuField);
MR.TE_GE               = MR.TE_GE_all(MR.simuField);
MR.TE                  = [MR.TE_SE;MR.TE_GE];

% Orientation of patch of cortex
MR.orientationOfCortex = 'random';
% MR.orientationOfCortex = 'perpendicular';
MR.T1_ex              = 1.950;
MR.T1_in              = 2.200;
MR.TR                 = 2.5;
MR.TI                 = 1.1;
MR.inv_eff            = 1;
%*************************************************************************
%% Visualise Results(VR)
%*************************************************************************
% Kernel to emulate results obtained with 0.75mm isotropic voxel (from Koopmans2011)
VR.convKernel         = [.25 .5 .25];  

% Compartments to simulate
IdxUp                 = VM.nMicroVascComp + VM.nICA+ VM.nICV + VM.simuExtraCortVein;
VR.allVasc            = boolean(ones(1,IdxUp));
VR.microVasc          = [true true true true true false false false false false false false false];     % macrovasc, defined as everything that does not belong to the mesh
VR.macroVasc          = ~ VR.microVasc;
VR.ICVvasc            = [false false false false false false false false false true true true true];
VR.ICAvasc            = [false false false false false true true true true false false false false];
VR.matrixVasc         = VR.allVasc(1:IdxUp);    
clear IdxUp

% Revise names if section above is modified 
VR.matrixVasc_name    = struct([]);
VR.matrixVasc_name{1} = 'AllVasculature';
VR.matrixVasc_name{2} = 'LaminarVasculature';
VR.matrixVasc_name{3} = 'ICV';
VR.colorStr           = ['blue   '; 'green  '; 'red    '; 'cyan   '; 'magenta'; 'yellow '; 'black  '; 'cyan   '; 'magenta'];

% Define folder names to save the results
VR.folder_name        = 'simulationResults'; 
VR.folder_name        = fullfile(pwd, VR.folder_name);
mkdir(VR.folder_name)

% Show signal change (deltaS) or signal change normalised (deltaS/S)
VR.showDeltaS         = 0; % 1: deltaS, 0: deltaS/S

% Define axis names
if VM.simuExtraCortVein == 0
    VR.layerAxis = (0:(1/(VM.nLayers-1)):1);
%     VR.layerAxis_conv = (0:(1/(VM.nLayers + length(VR.convKernel)-2)):1);

VR.layerAxis_conv = (0:(1/(VM.nLayers-3)):1);   
    VR.xLabelStr = 'WM/GM                  Relative cortical depth               GM/CSF';
else
    VR.layerAxis = (0:(1/(VM.nLayers-1)): 1+ (1/(VM.nLayers-1)));
    VR.layerAxis_conv = (0:(1/(VM.nLayers + length(VR.convKernel)-2)):1+(1/(VM.nLayers + length(VR.convKernel)-2)));
    VR.xLabelStr = 'WM/GM                  Relative cortical depth                 CSF';    
end

if VR.showDeltaS
    VR.yLabelStr = '\Delta S [a.u.]';
else
    VR.yLabelStr = '\Delta S/S [%]';
end

% BOLD and VASO laminar signal change obtained from our imaging data 
VR.VASO_normalized_to_base  = [-0.8509   -1.0144   -1.0855   -1.2499   -1.2345   -1.1781   -1.0622   -0.8527];            % VASO mean signal change averaged across all participants. The first and the last data points have been removed. 
VR.err_bar_VASO             = [ 0.1933    0.1931    0.1806    0.1842    0.1814    0.1776    0.1796    0.1668];            % standard error of the mean 
VR.BOLD_normalized_to_base  = [3.6238    4.2873    4.7516    5.4102    5.8935    6.4859    7.1249    8.0727];             % BOLD mean signal change averaged across all participants. The first and the last data points have been removed. 
VR.err_bar_BOLD             = [0.5555    0.6712    0.8160    0.9531    1.0261    1.0996    1.2108    1.5013];             % standar error of the mean 

       % if no convolution
%   VR.VASO_normalized_to_base = [-0.7223   -0.8509   -1.0144   -1.0855   -1.2499   -1.2345   -1.1781   -1.0622   -0.8527   -0.6904];
%   VR.err_bar_VASO =[0.1823    0.1933    0.1931    0.1806    0.1842    0.1814    0.1776    0.1796    0.1668    0.1817];
%   VR.BOLD_normalized_to_base =[3.0962    3.6238    4.2873    4.7516    5.4102    5.8935    6.4859    7.1249    8.0727    8.9206];
%   VR.err_bar_BOLD = [0.4687    0.5555    0.6712    0.8160    0.9531    1.0261    1.0996    1.2108    1.5013    1.8632];
% Information appeared on the command window
VR.separatingAsterisks = '********************************************';

%*************************************************************************
%% Vascular Model Output (VMout)
%*************************************************************************
% This struct is empty at initialization
VMout.ErrorLeakingMethod   = []; 
% VMout.da: drained area

%*************************************************************************
%% Setting specific simulation parameters
%*************************************************************************

if (strcmp(MR.IV_R2_mode, 'bloodR2revised'))                                    % Revised values available only for 3T and 7T 
    MR.simuField = logical([0 0 0 0 1 0 0 0 0]);
end

if (strcmp(MR.simuMode , 'multiTE'))                                            % Experimental multiecho data in Poser2009 and Koopmans2011 are at 7T, TEs scaled with field strength
    MR.multipleEchoes = MR.multiEcho_7T' * (7./MR.B0);
end


%% Show status in the Command Window
fprintf('\n%s\n%s\n%s\n',VR.separatingAsterisks,'Initialization of the parameters completed',VR.separatingAsterisks)



