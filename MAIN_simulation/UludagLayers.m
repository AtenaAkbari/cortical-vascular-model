% 
% ULUDAGLAYERS
%
% Author: Irati Markuerkiaga, 24.09.2013
% VASO addition: Atena Akbari

%%
% DEFINITION
% S_0:              Proton density signal
% S_in:             Intravascular MR signal
% S_ex:             Extravascular MR signal
% S_tot:            Total MR signal (at rest, maybe put an r into the name)
% S_tot_act:        Total MR signal upon neural activation
% S_BOLD:           BOLD signal (percentage of increased signal from rest to active state)
% Phi:              Ratio of the IV/EV proton densities. Sligthly bigger than 1 for GM, but its value can be decreased when bi-polar gradients used
% CBV:              Fractional blood volume in relation to whole tissue volume (usually around 1-5%)
% dCBV_act:         Change in CBV upon activation (it's a fraction, different for the different vessel compartments)
% d_ex:             Extra vascular compression factor (i.e. how big are the changes in the amount of protons in the extra-vascular space due to enlargement of the vessel), d=1 most plausible
% state:            Neural state: 'active' or 'rest'
function [UludagLayers_o, NVC, actChange]= UludagLayers(VM, NVC, MR, VMout,VR, curLayer, modality)

% For VASO:
if (strcmp(modality, 'VASO'))
% Read the parameters for the current layer
if ~(curLayer> VM.nLayers)
    capVolumeFraction  = VMout.meshVolumeFraction(curLayer);
    f_ICV              = VMout.f_ICV(:,curLayer)';
    f_ICA              = VMout.f_ICA(:,curLayer)';
    veinDiam_1         = VMout.veinDiam_1(:,curLayer)';
    artDiam_1          = VMout.artDiam_1(:,curLayer)';
    veinOxSat_act      = VMout.veinOxSat_act(:,curLayer)';
    artOxSat_act       = VMout.artOxSat_act(:,curLayer)';
end

% Load oxygenation and volume values for each compartment both at rest and activation
[Act_vol_oxy, NVC] = getAct_vol_oxy(capVolumeFraction, f_ICV, f_ICA, NVC, VR,curLayer, 'VASO');
Act_vol_oxy_veins = ones(length(f_ICV),1).*Act_vol_oxy(end,:,:,:,:,:);
Act_vol_oxy_art   = ones(length(f_ICA),1).*Act_vol_oxy(end-1,:,:,:,:,:);
Act_vol_oxy_veins (:,[1 2 5],:,:,:,:) =repmat([veinDiam_1' f_ICV' veinOxSat_act'],1,1,length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range));
Act_vol_oxy_art (:,[1 2 5],:,:,:,:) =repmat([artDiam_1' f_ICA' artOxSat_act'],1,1,length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range));
Act_vol_oxy_veins(Act_vol_oxy_veins(:,2)==0,:) =0;
Act_vol_oxy_art(Act_vol_oxy_art(:,2)==0,:)     =0;

if curLayer ==1 || curLayer ==2 || curLayer ==3
        Act_vol_oxy_veins(2:end,:,:,:,:,:) =[];
        Act_vol_oxy_art(2:end,:,:,:,:,:)   =[];
end
    
if curLayer ==4 || curLayer ==5 || curLayer ==6  || curLayer ==7
        Act_vol_oxy_veins(3:end,:,:,:,:,:) =[];
        Act_vol_oxy_art(3:end,:,:,:,:,:)   =[];
end

if curLayer ==8 || curLayer ==9
        Act_vol_oxy_veins(4,:,:,:,:,:)     =[];
        Act_vol_oxy_art(4,:,:,:,:,:)       =[];
end
    
Act_vol_oxy  = [Act_vol_oxy((1:(end-2)),:,:,:,:,:) ; Act_vol_oxy_art; Act_vol_oxy_veins];
CBV                   = Act_vol_oxy(:, 2,1); 
dCBV_act              = Act_vol_oxy(:, 3,:,:,:,:);
CBV_act               = CBV.* (1+dCBV_act);
actChange.Act_vol_oxy = Act_vol_oxy;
actChange.dCBV_act    = dCBV_act;
NVC.nCompartments = length(CBV_act); 

% Compute VASO signal for each field strength
for curB0 = 1 : length(MR.B0)
    % REST
    [~, ~, S_nulled_VASO, S_BC_VASO, R2_in_sum, R2_ex_sum, ...
    R2_tissue, R2_blood, R2_blood_O2, R2_tissue_O2, delta_vs_base]  = ...
    getRelaxedS(VM, NVC, MR, curB0, CBV, Act_vol_oxy, 'rest', 'VASO', curLayer, []);

    S_tot_VASO_BC     = (1-sum(CBV)).* S_BC_VASO ;    % BOLD-Corrected VASO
    S_tot_VASO_nulled = (1-sum(CBV)).* S_nulled_VASO;    % Tissue signal at the time of blood nulling 
    
    % ACTIVATION
    [~, ~, S_nulled_act_VASO, S_BC_act_VASO, R2_in_sum_act, R2_ex_sum_act, R2_tissue_act, R2_blood_act,...
        R2_blood_O2_act, R2_tissue_O2_act, delta_vs_act] = getRelaxedS( VM, NVC, MR, curB0, CBV_act, ...
        Act_vol_oxy, 'active', 'VASO', curLayer);
    
    S_tot_act_VASO_BC     = (1 -(sum(CBV.*( 1+ NVC.d_ex*dCBV_act)))) .* S_BC_act_VASO;
    S_tot_act_VASO_nulled = (1 -(sum(CBV.*( 1+ NVC.d_ex*dCBV_act)))) .* S_nulled_act_VASO;
    
    % Total VASO SIGNAL
    S_VASO_BC     = (S_tot_act_VASO_BC - S_tot_VASO_BC)./ S_tot_VASO_BC;
    S_VASO_nulled = (S_tot_act_VASO_nulled - S_tot_VASO_nulled)./ S_tot_VASO_nulled;
    
    
    %% Store results for plotting purposes
    % REST
    UludagLayers_o.S_R2compartment.S_nulled_array_VASO = S_nulled_VASO;
    UludagLayers_o.S_R2compartment.S_BC_array_VASO     = S_BC_VASO;    
    UludagLayers_o.S_R2compartment.R2_in_sum_array     = R2_in_sum;
    UludagLayers_o.S_R2compartment.R2_ex_sum_array     = R2_ex_sum;
    UludagLayers_o.S_R2compartment.R2_tissue_array     = R2_tissue;
    UludagLayers_o.S_R2compartment.R2_blood_array      = R2_blood;
    UludagLayers_o.S_R2compartment.R2_blood_O2_array   = R2_blood_O2;
    UludagLayers_o.S_R2compartment.R2_tissue_O2_array  = permute(R2_tissue_O2, [2 1]);
    UludagLayers_o.S_R2compartment.S_tot_BC_array_VASO = S_tot_VASO_BC;
    UludagLayers_o.S_R2compartment.delta_vs_base       = delta_vs_base;
    
    % ACTIVATION
    UludagLayers_o.S_R2compartment_act.S_nulled_act_array_VASO = S_nulled_act_VASO;
    UludagLayers_o.S_R2compartment_act.S_BC_act_array_VASO     = S_BC_act_VASO;
    UludagLayers_o.S_R2compartment_act.R2_in_sum_act_array     = R2_in_sum_act;
    UludagLayers_o.S_R2compartment_act.R2_ex_sum_act_array     = R2_ex_sum_act;
    UludagLayers_o.S_R2compartment_act.R2_tissue_act_array     = R2_tissue_act;
    UludagLayers_o.S_R2compartment_act.R2_blood_act_array      = R2_blood_act;
    UludagLayers_o.S_R2compartment_act.R2_blood_O2_act_array   = R2_blood_O2_act;
    UludagLayers_o.S_R2compartment_act.R2_tissue_O2_act_array  = permute(R2_tissue_O2_act, [2 1 3 4 5 6]);
    UludagLayers_o.S_R2compartment_act.S_tot_act_array_VASO_BC = S_tot_act_VASO_BC;
    UludagLayers_o.S_R2compartment_act.delta_vs_act            = delta_vs_act;
    
    % Total VASO SIGNAL
    UludagLayers_o.S_VASO_BC_array  = S_VASO_BC;
    UludagLayers_o.S_VASO_nulled_array  = S_VASO_nulled;
end

% For BOLD:
elseif  (strcmp(modality, 'BOLD'))
    if ~(curLayer> VM.nLayers)
    capVolumeFraction = VMout.meshVolumeFraction(curLayer);
    f_ICV = VMout.f_ICV(:,curLayer)';
    f_ICA = VMout.f_ICA(:,curLayer)';
    veinDiam_1 = VMout.veinDiam_1(:,curLayer)';
    artDiam_1 = VMout.artDiam_1(:,curLayer)';
    veinOxSat_act = VMout.veinOxSat_act(:,curLayer,:);
    artOxSat_act = VMout.artOxSat_act(:,curLayer)';
    end
% Load CBV and oxygenation values in the voxel
[Act_vol_oxy, NVC] = getAct_vol_oxy(capVolumeFraction, f_ICV, f_ICA, NVC, VR,curLayer, 'BOLD');
Act_vol_oxy_veins = ones(length(f_ICV),1).*Act_vol_oxy(end,:,1:16:16,:);
Act_vol_oxy_art = ones(length(f_ICA),1).*Act_vol_oxy(end-1,:,1:16:16,:);
Act_vol_oxy_veins = ones(length(f_ICV),1).*Act_vol_oxy(end,:,:,:);
Act_vol_oxy_art = ones(length(f_ICA),1).*Act_vol_oxy(end-1,:,:,:);
Act_vol_oxy_veins (:,[1 2],:,:) = reshape((repmat([veinDiam_1' f_ICV'],1,16,16)),[length(f_ICV) 2 16 16]);
Act_vol_oxy_art (:,[1 2],:,:) = reshape((repmat([artDiam_1' f_ICA'],1,16,16)),[length(f_ICA) 2 16 16]);
Act_vol_oxy_veins(Act_vol_oxy_veins(:,2)==0,:)=0;
Act_vol_oxy_art(Act_vol_oxy_art(:,2)==0,:)=0;

if curLayer ==1 || curLayer ==2 || curLayer ==3
    Act_vol_oxy_veins(2:end,:,:,:)=[];
    Act_vol_oxy_art(2:end,:,:,:)=[];
        
end
    
if curLayer ==4 || curLayer ==5 || curLayer ==6  || curLayer ==7
    Act_vol_oxy_veins(3:end,:,:,:)=[];
    Act_vol_oxy_art(3:end,:,:,:)=[];
end
    
if curLayer ==8 || curLayer ==9
    Act_vol_oxy_veins(4,:,:,:)=[];
    Act_vol_oxy_art(4,:,:,:)=[];
end
    
Act_vol_oxy = [Act_vol_oxy((1:(end-2)),:,:,:) ; Act_vol_oxy_veins; Act_vol_oxy_art];
CBV = Act_vol_oxy(:, 2); 
dCBV_act = Act_vol_oxy(:, 3);
CBV_act = CBV.* (1+dCBV_act);
actChange.Act_vol_oxy = Act_vol_oxy;
actChange.dCBV_act = dCBV_act;
NVC.nCompartments = length(CBV_act); 

% Compute BOLD signal for each field strength
for curB0 = 1 : length(MR.B0)
    % REST
    [S_in_BOLD, S_ex_BOLD, ~, ~, R2_in_sum, R2_ex_sum, R2_tissue, R2_blood, ...
        R2_blood_O2, R2_tissue_O2, delta_vs_base] = ...
        getRelaxedS( VM, NVC, MR, curB0, CBV, Act_vol_oxy, 'rest', 'BOLD', curLayer, []);

        S_tot_BOLD = (1-sum(CBV)).* S_ex_BOLD + (NVC.Phi* S_in_BOLD) .* CBV;  

    % ACTIVATION
    [S_in_act_BOLD, S_ex_act_BOLD, ~, ~, R2_in_sum_act, R2_ex_sum_act,...
        R2_tissue_act, R2_blood_act, R2_blood_O2_act, R2_tissue_O2_act, delta_vs_act] = ...
        getRelaxedS( VM, NVC, MR, curB0, CBV_act, Act_vol_oxy, 'active', 'BOLD', curLayer);
    
    S_tot_act_BOLD = (1 -(sum(CBV.*( 1+ NVC.d_ex*dCBV_act)))) .* S_ex_act_BOLD + NVC.Phi *S_in_act_BOLD.*(CBV.*(1+dCBV_act));
    S_BOLD = (S_tot_act_BOLD - S_tot_BOLD)./ S_tot_BOLD;

    %% Store results for plotting purposes
    % REST
    UludagLayers_o.S_R2compartment.S_in_array_BOLD = permute(S_in_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment.S_ex_array_BOLD = permute(S_ex_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment.R2_in_sum_array = permute(R2_in_sum, [2 1]);
    UludagLayers_o.S_R2compartment.R2_ex_sum_array = permute(R2_ex_sum, [2 1]);
    UludagLayers_o.S_R2compartment.R2_tissue_array = R2_tissue;
    UludagLayers_o.S_R2compartment.R2_blood_array = R2_blood;
    UludagLayers_o.S_R2compartment.R2_blood_O2_array = permute(R2_blood_O2, [2 1]);
    UludagLayers_o.S_R2compartment.R2_tissue_O2_array= permute(R2_tissue_O2, [2 1]);
    UludagLayers_o.S_R2compartment.S_tot_array_BOLD = permute(S_tot_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment.delta_vs_base = permute(delta_vs_base, [2 1]);
    
    % ACTIVATION
    UludagLayers_o.S_R2compartment_act.S_in_act_array_BOLD = permute(S_in_act_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment_act.S_ex_act_array_BOLD = permute(S_ex_act_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment_act.R2_in_sum_act_array = permute(R2_in_sum_act, [2 1]);
    UludagLayers_o.S_R2compartment_act.R2_ex_sum_act_array = permute(R2_ex_sum_act, [2 1]);
    UludagLayers_o.S_R2compartment_act.R2_tissue_act_array = R2_tissue_act;
    UludagLayers_o.S_R2compartment_act.R2_blood_act_array= R2_blood_act;
    UludagLayers_o.S_R2compartment_act.R2_blood_O2_act_array = permute(R2_blood_O2_act, [2 1]);
    UludagLayers_o.S_R2compartment_act.R2_tissue_O2_act_array= permute(R2_tissue_O2_act, [2 1]);
    UludagLayers_o.S_R2compartment_act.S_tot_act_array_BOLD = permute(S_tot_act_BOLD, [2 1]);
    UludagLayers_o.S_R2compartment_act.delta_vs_act = permute(delta_vs_act, [2 1]);
    
    % Total BOLD SIGNAL
    UludagLayers_o.S_BOLD_array= permute(S_BOLD, [2 1]);
    UludagLayers_o.Act_vol_oxy_all = Act_vol_oxy;
end
end
end
