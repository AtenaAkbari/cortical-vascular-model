%%
% BREAKDOWNBOLDSIGNAL
%
% Author: Irati Markuerkiaga
% VASO addition : Atena Akbari
% Description: Compute mircro/macro- and intra/extra-vascular contributions to the BOLD signal
%
% ASSUMPTIONS/SIMPLIFICATIONS
%
% COMMENTS
% - Variable vasc (1: Total, 2: microVasc, 3: macroVasc) has to be changed
% manually when plotting the BOLD signal
% - Variables of the loops: - h: 1)totalVasc, 2)microVasc, 3)macroVasc
%                           - j: Layer number: 1) WM, 5)CSF
%                           - k: B0

%%
function VR = breakDownSignal( VM, NVC, MR, VR, VMout, layerOut, modality)

% Get number of ICVs and ICAs per simulated layer
nICV_layerVox     = sum((VMout.f_ICV>0),1);
nICA_layerVox     = sum((VMout.f_ICA>0),1);

if (strcmp(modality, 'VASO'))
for j = 1 : length(layerOut)
    S_R2compartment_Layer(j)     = layerOut(j).data.S_R2compartment;
    S_R2compartment_act_Layer(j) =  layerOut(j).data.S_R2compartment_act;
end

h=1; % total vasculature
vascSim = VR.matrixVasc(h,:);
for j = 1 : (length(S_R2compartment_Layer)-VM.simuExtraCortVein)
    % Obtain compartment Volume
    CapBedCompartments    = getAct_vol_oxy(VMout.meshVolumeFraction(j),VMout.f_ICA(:,j), VMout.f_ICV(:,j), NVC, VR, j, modality);                                 
    d_CBV                 = CapBedCompartments (:,3,:,:,:,:);
    nrLaminComparts       = VM.nMicroVascComp;            
    simuVasc              = vascSim(1:nrLaminComparts + nICA_layerVox(j)+ nICV_layerVox(j) );
    CapBedCompartments    = CapBedCompartments((1:nrLaminComparts), 2);
    a                     = VMout.f_ICV(:,j);
    b                     = VMout.f_ICA(:,j);
    a(VMout.f_ICV(:,j)==0)= [];
    b(VMout.f_ICA(:,j)==0)= [];
    d_CBV                 = [d_CBV(1:nrLaminComparts,:,:,:,:,:);ones(length(b),1,length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range)).*d_CBV(end-1,:,:,:,:,:);ones(length(a),1,length(NVC.delta_CBV_range),length(NVC.delta_CBV_range),length(NVC.delta_CBV_range)).*d_CBV(end,:,:,:,:,:)];
    CBV                   = [CapBedCompartments ; b;a]; 
    d_CBV_vasc            = d_CBV(simuVasc,:,:,:,:,:);
    d_CBV_vasc            = reshape (d_CBV_vasc, [length(simuVasc), length(NVC.delta_CBV_range)^4]);
    CBV_vasc              = CBV(simuVasc);
    VR.d_CBV              = d_CBV_vasc;
    
    for k = 1 : length(MR.B0)                               
        % Extravascular Signal at baseline
        R_ex_BOLD     = S_R2compartment_Layer(j).R2_tissue_array(1,k) + sum(S_R2compartment_Layer(j).R2_tissue_O2_array(1,simuVasc,k));
        S_nulled_VASO = (NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+ MR.inv_eff.*exp(-MR.TR/MR.T1_ex))*exp(-(R_ex_BOLD* MR.TE(2,k)));  
        S_BC_VASO     = NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+MR.inv_eff.*exp(-MR.TR/MR.T1_ex);  
               
        % Extravascular Signal upon activation
        R_ex_act          = S_R2compartment_act_Layer(j).R2_tissue_act_array(1,k) + sum(S_R2compartment_act_Layer(j).R2_tissue_O2_act_array(1,simuVasc,k));
        S_BC_act_VASO     = NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+MR.inv_eff.*exp(-MR.TR/MR.T1_ex);                
        S_nulled_act_VASO = (NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+MR.inv_eff.*exp(-MR.TR/MR.T1_ex))* exp(-(R_ex_act* MR.TE(2,k)));   
        
        % Total signal at rest 
        S_tot_BC_VASO     = S_BC_VASO.*(1-sum(CBV_vasc));
        S_tot_nulled_VASO = S_nulled_VASO .*(1-sum(CBV_vasc));

        % Total signal at activity 
        S_tot_BC_act_VASO     = S_BC_act_VASO.*(1-sum(CBV_vasc .*(1+d_CBV_vasc)));
        S_tot_act_nulled_VASO = S_nulled_act_VASO .* (1-sum(CBV_vasc .*(1+d_CBV_vasc)));

        % Return Parameters to simulate VASO and BOLD
        VR.S_VASO    (h,1,j,k,1:length(NVC.delta_CBV_range)^4)     = (S_tot_BC_act_VASO - S_tot_BC_VASO)./S_tot_BC_VASO;
        VR.S_VASO_BC (h,1,j,k,1:length(NVC.delta_CBV_range)^4)     = (S_BC_act_VASO .* (1- sum(CBV_vasc.*(1+d_CBV_vasc))) - ...
            S_BC_VASO .* (1-sum(CBV_vasc)))/S_tot_BC_VASO;
        VR.S_nulled_VASO (h,1,j,k,1:length(NVC.delta_CBV_range)^4) = (S_nulled_act_VASO.* (1- sum(CBV_vasc.*(1+d_CBV_vasc))) - ...
            S_nulled_VASO.* (1-sum(CBV_vasc)))/S_tot_nulled_VASO;
        VR.S_BC_VASO (h,1,j,k) = S_BC_VASO;
        VR.S_nulled_VASO (h,1,j,k) = S_nulled_VASO;
        VR.CBV_vasc  (h,1,j,k,1:length(CBV_vasc)) = CBV_vasc;
        VR.S_tot_BC_VASO(h,1,j,k,length(NVC.delta_CBV_range)^4) = S_tot_BC_VASO;
        VR.S_BC_act_VASO (h,1,j,k) = S_BC_act_VASO;
        VR.S_nulled_VASO (h,1,j,k) = S_nulled_act_VASO;
        VR.S_tot_act_BC_VASO(h,1,j,k,1:length(NVC.delta_CBV_range)^4) = S_tot_BC_act_VASO;
        VR.S_tot_act_nulled_VASO(h,1,j,k,1:length(NVC.delta_CBV_range)^4)= S_tot_act_nulled_VASO;
    end
end
        
elseif (strcmp(modality, 'BOLD'))
    VR.OXY = reshape(layerOut(1).data.Act_vol_oxy_all, [7 5 256]);
    h=1; % total vasculature
    vascSim = VR.matrixVasc(h,:); 
    
    for j = 1 : length(layerOut)
        S_R2compartment_Layer(j)     = layerOut(j).data.S_R2compartment;
        S_R2compartment_act_Layer(j) =  layerOut(j).data.S_R2compartment_act;
    end
    
    
for j = 1 : (length(S_R2compartment_Layer)-VM.simuExtraCortVein)
    
   % Obtain compartments volume
    CapBedCompartments = getAct_vol_oxy(VMout.meshVolumeFraction(j), VMout.f_ICV(:,j),  VMout.f_ICA(:,j),NVC,VR, j,modality);                                 
    d_CBV = CapBedCompartments (:,3);
    nrLaminComparts =VM.nMicroVascComp;            
    simuVasc = vascSim(1:nrLaminComparts+nICV_layerVox(j)+nICA_layerVox(j));
    CapBedCompartments = CapBedCompartments((1:nrLaminComparts), 2);
    a = VMout.f_ICV(:,j);
    b = VMout.f_ICA(:,j);
    a(VMout.f_ICV(:,j)==0)= [];
    b(VMout.f_ICA(:,j)==0)= [];
    d_CBV = [d_CBV(1:nrLaminComparts); ones(length(b),1)*d_CBV(end-1);ones(length(a),1)*d_CBV(end)];
    CBV = [CapBedCompartments ; b;a];
    d_CBV_vasc = d_CBV(simuVasc);
    CBV_vasc = CBV(simuVasc);     
    
    for k = 1 : length(MR.B0)
        % Intravascular Signal                
        R_in_BOLD = S_R2compartment_Layer(j).R2_in_sum_array(:,simuVasc,k);
        S_in_BOLD = NVC.Rho_bl * exp(-(R_in_BOLD* MR.TE(2,k)));
        R_in_act_BOLD = S_R2compartment_act_Layer(j).R2_in_sum_act_array(:,simuVasc,k);
        S_in_act_BOLD = NVC.Rho_bl * exp(-(R_in_act_BOLD* MR.TE(2,k)));
        
        % Extravascular Signal
        R_ex_BOLD = S_R2compartment_Layer(j).R2_tissue_array(1,k) + sum(S_R2compartment_Layer(j).R2_tissue_O2_array(:,simuVasc,k),2);
        S_ex_BOLD = NVC.Rho_GM * exp(-(R_ex_BOLD* MR.TE(2,k)));
        R_ex_act = S_R2compartment_act_Layer(j).R2_tissue_act_array(1,k) + sum(S_R2compartment_act_Layer(j).R2_tissue_O2_act_array(:,simuVasc,k),2);
        S_ex_act_BOLD = NVC.Rho_GM * exp(-(R_ex_act* MR.TE(2,k)));
        
        % Total BOLD signal
        S_tot_BOLD = S_ex_BOLD*(1-sum(CBV_vasc))+S_in_BOLD * CBV_vasc;
        S_tot_act_BOLD = S_ex_act_BOLD .* (1-sum(CBV_vasc .* (1+d_CBV_vasc))) + S_in_act_BOLD * (CBV_vasc .* (1+d_CBV_vasc));
        
        % Return Parameters to simulate VASO and BOLD
        VR.S_BOLD    (h,1,j,k,1:256) = (S_tot_act_BOLD - S_tot_BOLD)./S_tot_BOLD;
        VR.S_BOLD_in (h,1,j,k,1:256) = (S_in_act_BOLD * (CBV_vasc.*(1+d_CBV_vasc)) - S_in_BOLD * CBV_vasc)./S_tot_BOLD;
        VR.S_BOLD_ex (h,1,j,k,1:256) =  (S_ex_act_BOLD * (1- sum(CBV_vasc.*(1+d_CBV_vasc))) - S_ex_BOLD * (1-sum(CBV_vasc)))./S_tot_BOLD;
%         VR.S_in_BOLD (h,i,j,k,1:length(S_in_BOLD)) = S_in_BOLD;
        VR.S_ex_BOLD (h,1,j,k,1:256) = S_ex_BOLD;
        VR.CBV_vasc  (h,1,j,k,1:length(CBV_vasc)) = CBV_vasc;
%         VR.S_in_act_BOLD (h,1,j,k,1:length(S_in_act_BOLD)) = S_in_act_BOLD;
        VR.S_ex_act_BOLD (h,1,j,k,1:256) = S_ex_act_BOLD;
        VR.S_tot_act_BOLD(h,1,j,k,1:256) = S_tot_act_BOLD;
    end
end
end
end

