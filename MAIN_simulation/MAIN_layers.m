%%
% MAIN_LAYERS_BOLD
%
% Author: Irati Markuerkiaga
% Atena Akbari modeified this to simulate VASO in addition to the BOLD
% signal 
%
% Description: Main script of the layer BOLD simulation project.
% Blocks of the file:
% 1- Initialize simulation parameters
% 2- Build Vascular Model
% 3- Predict the BOLD signal across the cortex
% 4- Visualise and save results
%
% NOTE: 
% - Blocks need to be run in the order presented in this file

%%
clear;
close all;
clc;

%% INITIALIZE MAIN SIMULATION VARIABLES
tic;
initSimulSettings;

%% BUILD VASCULAR MODEL OF THE CORTEX
VMout = buildVascularModel_1xG4G3_2xA4A3(VM, NVC, VR);

%% PREDICT VASO SIGNAL ACROSS LAYERS UPON ACTIVATION
[layersOut, NVC] = SignalAcrossLayers(VM, NVC, MR, VMout, VR, 'VASO'); 
VR = breakDownSignal( VM, NVC, MR, VR, VMout, layersOut, 'VASO');
VR =  plot_VASOprofiles (MR, VR, NVC);

%% PREDICT BOLD SIGNAL ACROSS LAYERS UPON ACTIVATION 
[layersOut, NVC] = SignalAcrossLayers(VM, NVC, MR, VMout, VR, 'BOLD'); 
VR = breakDownSignal( VM, NVC, MR, VR, VMout, layersOut, 'BOLD');
plot_BOLDprofiles (MR, VR, NVC)

%% SAVE PARAMETERS AND RESULTS OF THE SIMULATION
save(fullfile(VR.folder_name, 'simulation_res_param'), 'MR', 'NVC', 'VM', 'VMout', 'VR', 'layersOut')
fprintf('All simulation parameters, results and figures are stored in %s\n', VR.folder_name)
fprintf('\n')
