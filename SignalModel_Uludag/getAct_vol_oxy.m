%%
% Irati Markuerkiaga, 08.10.2013
% Table with blood volume and blood oxigenation values for the different vascular compartments for resting state and activation

% The structure of the table is the following:
%   - Lines:        1. Arterioles macrovasc
%                   2. Arterioles microvasc
%                   3. Capillaries
%                   4. Venules microvasc
%                   5. Venules macrovasc
%                   6. Macrovasculature
%   - Column 1: Diameter of the vessels [um]
%   - Column 2: Blood volume
%   - Column 3: Volume change ratio upon activation
%   - Column 4: Blood oxygenation in rest
%   - Column 5: Blood oxygenation upon neural activation
%
% The diameters and volumes of the different compartments have been
% obtained from Boas2008
%
% 3 microvascular dilation models
% Model activation under normoxia or hyperoxia
%
% Assumptions for modelling different stimulus strengths:
%
% 1) There is a 1:1 relation between neural activity and oxygen extraction.
% At full activation, the increase in O2 consumption is 12.5% -> b1 =
% 0.125. If the activation is halved, O2 consumption will be halved
%
% 2) The relationship between activation and deltaCBF follows an
% exponential pattern, extrapolated from Chiacchiaretta 2013. At half of
% the activation, CBF is 0.7 times deltaCBFmax, which yields a constant tau
% = 2.45
%%
function[Act_vol_oxy_all, NVC] = getAct_vol_oxy(capVolumeFraction, f_ICV,f_ICA, NVC, VR, curLayer, modality)
% capVolumeFraction: Obtained from Weber2008
% f_ICV and f_ICA: Obtained from buildVascularModel.m
relStr = NVC.relStrength(curLayer);
relStr_ica = NVC.relStrength_ica(curLayer);
if (strcmp(modality, 'VASO'))
% mode 'Uludag2009'
% Blood volume and oxygenation values for baseline and activation
Act_vol_oxy_all     = zeros(7, 5, length(NVC.delta_CBV_range), length(NVC.delta_CBV_range),length(NVC.delta_CBV_range));
for k = 1:length(NVC.delta_CBV_range)
    for t = 1:length(NVC.delta_CBV_range)
        for l = 1:length(NVC.delta_CBV_range)
            for c = 1:length(NVC.delta_CBV_range)
            Act_vol_oxy  = [ 24.5, NVC.arteriolFrac_macro* capVolumeFraction, relStr*NVC.CBV_art(k)*NVC.isHiperoxia, 0.95, 1;
                12.8, NVC.arteriolFrac_micro* capVolumeFraction, relStr*NVC.CBV_art(k)*NVC.isHiperoxia, 0.95, 1;
                8, NVC.capFrac* capVolumeFraction, relStr*NVC.CBV_cap(k)*NVC.isHiperoxia, 0.85, 1;
                12.8, NVC.venuleFrac_micro* capVolumeFraction, relStr*NVC.CBV_ven(t)*NVC.isHiperoxia, 0.65, 0.78;
                24.5, NVC.venuleFrac_macro* capVolumeFraction, relStr*NVC.CBV_ven(t)*NVC.isHiperoxia, 0.65, 0.78;
                55  , sum(f_ICA), relStr_ica*NVC.CBV_ICA(l)*NVC.isHiperoxia, 0.95, 1;
                200 , sum(f_ICV), relStr_ica*NVC.CBV_ICA(c)*NVC.isHiperoxia,  0.60, 0.78];
            
            Act_vol_oxy_all(:,:,k,t,l,c) = Act_vol_oxy;
            end
       end
    end
end

elseif (strcmp(modality, 'BOLD'))
    Act_vol_oxy_all = zeros(7, 5, length(NVC.ven_oxy_base), length(NVC.ven_oxy_base));
    for k = 1:length(NVC.ven_oxy_base)
        for t = 1:length(NVC.ven_oxy_base)
            Act_vol_oxy = [ 24.5, NVC.arteriolFrac_macro* capVolumeFraction,VR.deltaCBV_bestFit(1,:)*NVC.isHiperoxia, .95, 1;    
                12.8, NVC.arteriolFrac_micro* capVolumeFraction, VR.deltaCBV_bestFit(2,:)*NVC.isHiperoxia, 0.95, 1;
                5, NVC.capFrac* capVolumeFraction, VR.deltaCBV_bestFit(3,:)*NVC.isHiperoxia, 0.85, 0.95;
                12.8, NVC.venuleFrac_micro* capVolumeFraction, VR.deltaCBV_bestFit(4,:)*NVC.isHiperoxia, NVC.ven_oxy_base(k), NVC.ven_oxy_act(t);
                24.5, NVC.venuleFrac_macro* capVolumeFraction, VR.deltaCBV_bestFit(5,:)*NVC.isHiperoxia, NVC.ven_oxy_base(k), NVC.ven_oxy_act(t);
                75, sum(f_ICA), VR.deltaCBV_bestFit(6,:)*NVC.isHiperoxia, 0.95 , 1;
                200, sum(f_ICV), VR.deltaCBV_bestFit(7,:)*NVC.isHiperoxia, NVC.ven_oxy_base(k), NVC.ven_oxy_act(t)];
            
            Act_vol_oxy_all(:,:,k,t) = Act_vol_oxy;
        end
    end
end
end
