%%
% GETINTRINSICBLOOD_GRE.m
%
% Irati Markuerkiaga, 22.10.2013
% Description: Function to get the intrinsic blood relaxation rate value for GE.
%
% COMMENTS:
% #1- Values read from Uludag2009
% #2- Values read from Uludag2009 and extrapolated so that there is still
% some MR signal left at 7T upon activation.
% #3-Values read from Uludag2009 and extrapolated so that there is still
% some MR signal left at 9T upon activation, as reported in Budde2013.
%
% NOTE: 
% - The relaxation rates for B0> 4.7 T have been set to an arbitrarily high value
% to null the signal in them (following Uludag2009).
% - Changed for 7 T and above so that the IV signal for GE does not disappear. I
% calculated the regression curve in Excel and extrapolated the value for 7
% T: 11.675B0 - 14.427 -> B0 = 7, R2_0star = 
% (See fitting in Extrapol_R2star.xlsx)
%
% TODO:
% - Find current values of blood T2 and calculate T2* values from those
% 
% Last modified: 29.10.2014

%%
function [R2_star_0in] = getIntrinsicBlood_GE (fieldStrength, MR)

B0_local = [1.5 3 4 4.7 7 9.4 11.7 14 16.1];     % Magnetic field[T]
if ~(strcmp(MR.IV_R2_mode, 'bloodR2revised'))
    % #1
    R2_star_0in_array = [7 14 32 45 1e6 1e6 1e6 1e6 1e6];     % Intrinsic Blood Relaxation values for GRE
else
    % #2
    R2_star_0in_array = [7 14 30 45 67 1e6 1e6 1e6 1e6];        % Value for 7T obtained by linear extrapolation from the previous values, beharbada linearra beharrean kuadratikoa egin beharko nuke
    
    % % #3
    % R2_star_0in_array = [7   14   30   45   67   95   1e6   1e6   1e6]; % Simulate GE@9.4T still has IV contribution
end
% Output
R2_star_0in = R2_star_0in_array(B0_local == fieldStrength);
end

