%%
% GETRELAXEDS
%
% Author: Irati Markuerkiaga, 08.10.2013
% 
% Description: obtain MR signal given physiological parameters
% 
% Description: The vessel orientation thetha, the layed back path due to diffusion l are
% taken into account in the fit values of the extravascular effect of blood oxygenation
%
% NOTES:
%  - To 2004.11.06: 'bloodR2revised' mode is only possible together with
%  NVC.dilationMode = 'Uludag20009' and Y_base= 0.6 and Y_act=0.7 and
%  NVC.relStrength = 1 (across the cortex).
% - From Krishnamurthy 2014 (7T, SE):
%       1/T2 = A + B* (1-Y) + C * (1-Y)^2, 
%       For Hct = 42% and B0 = 7T: A = 14.9, B = -17.6, C = 264
% - From Lu 2012 (3T, SE):
%       1/T2 = A + B* (1-Y) + C * (1-Y)^2
%       For different Hct values:
%       A = a1 + a2 * Hct + a3 * Hct^2, 
%       B = b1 * Hct + b2* Hct^2
%       C = c1 * Hct*(1-Hct)
%  These coefficients are given by function getLuCoeffs.
%%
% DEFINITION
% S_in:             Intravascular MR signal
% S_ex:             Extravascular MR signal
% S_0:              Proton density signal
% CBV:              Fractional blood volume in relation to whole tissue volume (usually around 1-5%)
% R2_0ex:           Intrinsic relaxation of the extravascular signal
% R2_star_0ex:      __ relaxation of the extravascular signal
% R2_0in:           Intrinsic relaxation of arterial blood (Y = 100%)
% R2Hb_in:          Relaxation rate induced by different oxygenation levels of blood.
% R2_star_Hb_in:    Relaxation rate induced by different oxygenation levels of blood for gradient echo.
% R2_star_Hb_ex:    Additional static relaxation produced by the deoxy-Hb (different for each of the vascular compartments)
% R2_Hb_ex:         Additional relaxation produced by the deoxy-Hb (different for each of the vascular compartments)
% R2_ex:            Extravascular relaxation
% dCBV_act:         Change in CBV upon activation (it's a fraction, different for the different compartments)
% Y:                O2 saturation of blood
% Yoff:             O2 saturation that produces no magnetic susceptibility
%                   difference between intra-vascular and extra-vascular fluids.

function[S_in_BOLD, S_ex_BOLD, S_nulled_VASO, S_BC_VASO, R2_in_sum, R2_ex_sum, ...
    R2_tissue, R2_blood, R2_blood_O2, R2_tissue_O2, ...
    delta_vs]  = getRelaxedS( VM, NVC, MR, curB0, CBV, Act_vol_oxy, state, modality, simuPial,curLayer)
                                                                            
% DEFINE CONSTANTS
B0 = MR.B0(curB0); 

if (strcmp(modality, 'VASO'))
    
    if (strcmp(state, 'rest'))
    Y = Act_vol_oxy(:, 4);
    if B0 == 3 || B0 == 7
        R2_in_vein = 1/(MR.KrishnaT2in((B0 == 7) + 1, 1)/10^3);
        R2star_in_vein = 1/(MR.BlockleyT2starin((B0 == 7) + 1, 1)/10^3);
    end
    
elseif(strcmp(state, 'active'))
    Y = Act_vol_oxy(:, 5);
    if B0 == 3 || B0 == 7
        R2_in_vein = 1/(MR.KrishnaT2in((B0 == 7) + 1, 2)/10^3);
        R2star_in_vein = 1/(MR.BlockleyT2starin((B0 == 7) + 1, 2)/10^3);
    end
else
    fprintf ('\n The state you introduced is neither "rest" nor "active". Please, check the spelling! \n')
   end

%% RELAXATION RATES [1/s]
%************************************************************************************************************
% BLOOD
%************************************************************************************************************
switch MR.IV_R2_mode
    case 'bloodR2standard'
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in         = 2.74 * B0 - 0.6;                      % SE
        R2_star_0in    = getIntrinsicBlood_GE (B0, MR);        % GE
        
        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in        = 12.67 * B0^2 *(1-Y).^2;                % SE
        R2_star_Hb_in  = getBloodOxygenCoeff_GRE(B0)*(1-Y).^2;  % GE
        
        % Total intravascular relaxation
        R2_in          = R2_0in + R2Hb_in;                      % SE
        if ((B0 > 4.7) && MR.T2blood_starAtHF)
%             R2_star_in = R2_in; % Following DN's opinion about R2*of blood
            R2_star_0in  = R2_0in*2;
            R2_star_in   = R2_star_0in + R2Hb_in;
        else
            R2_star_in   = R2_star_0in + R2_star_Hb_in;         % GE
        end
        
    case 'bloodR2revised'
        % SE: R2_in for Y~ 60% and 70% are taken from Krishnamurthy 2014,
        % the rest of the compartments are taken from Uludag2009
        
        % GE: R2_in for Y~ 60% and 70% are taken from Blockeley2008,
        % the rest of the compartments are taken from Uludag2009, but could
        % be read from Blockley2008
        
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in        = 2.74 * B0 - 0.6;                 % SE
        R2_star_0in   = getIntrinsicBlood_GE (B0, MR);   % GE
        
        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in       = 12.67 * B0^2 *(1-Y).^2;               % SE
        R2_star_Hb_in = getBloodOxygenCoeff_GE(B0)*(1-Y).^2;  % GE
        
        % SE
        if B0 == 3
            % From the fitting results from Lu 2012
            LuCoeffs = getLuCoeffs(1);
            A        = LuCoeffs(1) + LuCoeffs(2) * NVC.Hct + LuCoeffs(3) * NVC.Hct^2;
            B        = LuCoeffs(4) * NVC.Hct + LuCoeffs(5)* NVC.Hct^2;
            C        = LuCoeffs(6) * NVC.Hct*(1-NVC.Hct);
        else
            % From the fitting results from Krishnamurthy 2014
            A = 14.9;
            B = -17.6;
            C = 264;
        end
        R2_in = A + B* (1-Y) + C * (1-Y).^2;        % Equation used by the Texas group
        
        % GE
        R2_star_in = R2_star_0in + R2_star_Hb_in;   % GE
%         R2_star_in(4:end) = R2star_in_vein;       % GE values from Blockley2008 in the venous part
    case 'IV_R2starRevised'
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in                  = 2.74 * B0 - 0.6;                   % SE
        R2star_adjustmentFactor = 4;
        R2_star_0in             = R2_0in*R2star_adjustmentFactor;     % GE

        
        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in       = 12.67 * B0^2 *(1-Y).^2;         % SE
        R2_star_Hb_in = R2Hb_in;                        % GE
        
        % Total intravascular relaxation
        R2_in     = R2_0in + R2Hb_in;                   % SE
%         R2_star_in = R2_in;                           % GE, Following DN's opinion about R2*of blood
        R2_star_in = (R2_star_0in + R2_star_Hb_in);     % GE, experimental measurements of IV contribution
end
%************************************************************************************************************
% TISSUE
%************************************************************************************************************
% Intrinsic relaxation rates of tissue (at Y = 100%)
R2_0ex      = 1.74* B0 + 7.77;                            % [1/s] Result of fitting the experimental R2,0 shown in Table 1 in Uludag 2009
R2_star_0ex = 3.74* B0 + 7.77;                            % [1/s], data source for the fitting not reported
% R2_star_0ex = 3.74* B0 + 4.77;                          % [1/s], varied so that it corresponds to the mean signal change observed by PK through the cortex


% Extra-vascular relaxation rate as a function of O2
% saturation(Monte-Carlo), a coefficient in Uludag2009 has been changed
% because it does not correspond to the graphs he shows in the paper
delta_vs = NVC.deltaChi_0 /(4*pi)*NVC.Hct * abs(NVC.Yoff - Y)* MR.gamma* B0;

% if (simuPial)
%     CBV = min(CBV, 0.05); % The coefficients were calculated up to this volume in Uludag 2009
% end
fitCoeff_SE  = getHbExFitCoeff ('SE', length(Y), simuPial, VM, MR);
R2_Hb_ex     = (fitCoeff_SE.a.* delta_vs.^5 + fitCoeff_SE.b.* delta_vs.^4 + fitCoeff_SE.c.* delta_vs.^3 + fitCoeff_SE.d .* delta_vs.^2 + fitCoeff_SE.e.* delta_vs + fitCoeff_SE.f).* CBV * 100; % SE, *100, because the relaxation rate is given per % of CBV, chech why it's not CBV+dCBV  %Atena; I changed the matrix size here. i chose only 6 row ao 10 rows in fit coefficient. Check it later, it might be wrong

fitCoeff_GRE = getHbExFitCoeff ('GRE', length(Y), simuPial, VM, MR);% In GE I do not use the outcome of the MonteCarlo simulation
R2_star_Hb_ex = (fitCoeff_GRE.a .* delta_vs.^5 + fitCoeff_GRE.b .* delta_vs.^4 + fitCoeff_GRE.c .* delta_vs.^3 + fitCoeff_GRE.d .* delta_vs.^2 + fitCoeff_GRE.e .* delta_vs + fitCoeff_GRE.f) .* (CBV) * 100; % GRE, *100, because the relaxation rate is given per % of CBV, chech why it's not CBV+dCBV

% TOTAL EV RELAXATION
R2_ex      = R2_0ex + sum(R2_Hb_ex);
R2_star_ex = R2_star_0ex + sum(R2_star_Hb_ex);

% Koopmans R2_star values at rest across the cortex
% if(strcmp(MR.EV_R2_mode, 'LayerR2star'))
%     if (strcmp(state, 'rest'))
%         R2_star_ex       = getLayerR2star( B0, curLayer);
%         R2_star_ex_0_act = R2_star_ex - sum(R2_star_Hb_ex);
%     else
%         R2_star_ex       = R2_star_ex_0_act + sum(R2_star_Hb_ex);
%     end
%         R2_star_0ex      = R2_star_ex_0_act;
% end

%% RELAXED SIGNALS
S_nulled_GE_VASO = ((NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+ MR.inv_eff.*exp(-MR.TR/MR.T1_ex)))* exp(-(R2_star_ex* MR.TE_GE(curB0)));
S_BC_GRE_VASO    = (NVC.Rho_GM *(1-(1+MR.inv_eff).*exp(-MR.TI/MR.T1_ex))+ MR.inv_eff.*exp(-MR.TR/MR.T1_ex)); 
S_nulled_VASO    = S_nulled_GE_VASO;
S_BC_VASO        = S_BC_GRE_VASO;
R2_in_sum        = R2_star_in';
R2_ex_sum        = R2_star_ex;
R2_tissue        = R2_star_0ex;
R2_tissue_O2     = R2_star_Hb_ex;
R2_blood         =  R2_star_0in;
R2_blood_O2      = R2_star_Hb_in'; 
S_in_BOLD = [];
S_ex_BOLD =[];
end

%%
if (strcmp(modality, 'BOLD'))
if (strcmp(state, 'rest'))
    Y= squeeze(Act_vol_oxy(:, 4,:));
    if B0 == 3 || B0 == 7
        R2_in_vein = 1/(MR.KrishnaT2in((B0 == 7) + 1, 1)/10^3);
        R2star_in_vein = 1/(MR.BlockleyT2starin((B0 == 7) + 1, 1)/10^3);
    end
    
elseif(strcmp(state, 'active'))
    Y = squeeze(Act_vol_oxy(:, 5,:));
    if B0 == 3 || B0 == 7
        R2_in_vein = 1/(MR.KrishnaT2in((B0 == 7) + 1, 2)/10^3);
        R2star_in_vein = 1/(MR.BlockleyT2starin((B0 == 7) + 1, 2)/10^3);
    end
else
    fprintf ('\n The state you introduced is neither "rest" nor "active". Please, check the spelling! \n')
end

%% RELAXATION RATES [1/s]
%************************************************************************************************************
% BLOOD
%************************************************************************************************************
switch MR.IV_R2_mode
    case 'bloodR2standard'
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in = 2.74 * B0 - 0.6;                 % SE
        R2_star_0in = getIntrinsicBlood_GE (B0, MR);  % GE
        
        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in = 12.67 * B0^2 *(1-Y).^2;                     % SE
        R2_star_Hb_in_BOLD = getBloodOxygenCoeff_GRE(B0)*(1-Y).^2; % GE
        
        % Total intravascular relaxation
        R2_in = R2_0in + R2Hb_in;                 % SE
        if ((B0 > 4.7) && MR.T2blood_starAtHF)
            %             R2_star_in = R2_in; % Following DN's opinion about R2*of blood
            R2_star_0in = R2_0in*2;
            R2_star_in = R2_star_0in + R2Hb_in;
        else
            R2_star_in = R2_star_0in + R2_star_Hb_in; % GE
        end
        
    case 'bloodR2revised'
        % SE: R2_in for Y~ 60% and 70% are taken from Krishnamurthy 2014,
        % the rest of the compartments are taken from Uludag2009
        
        % GE: R2_in for Y~ 60% and 70% are taken from Blockeley2008,
        % the rest of the compartments are taken from Uludag2009, but could
        % be read from Blockley2008
        
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in = 2.74 * B0 - 0.6;                 % SE
        R2_star_0in = getIntrinsicBlood_GE (B0, MR);  % GE
        
        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in = 12.67 * B0^2 *(1-Y).^2;         % SE
        R2_star_Hb_in = getBloodOxygenCoeff_GE(B0)*(1-Y).^2; % GE
        
        % SE
        if B0 == 3
            % From the fitting results from Lu 2012
            LuCoeffs = getLuCoeffs(1);
            A = LuCoeffs(1) + LuCoeffs(2) * NVC.Hct + LuCoeffs(3) * NVC.Hct^2;
            B = LuCoeffs(4) * NVC.Hct + LuCoeffs(5)* NVC.Hct^2;
            C = LuCoeffs(6) * NVC.Hct*(1-NVC.Hct);
        else
            % From the fitting results from Krishnamurthy 2014
            A = 14.9;
            B = -17.6;
            C = 264;
        end
        R2_in = A + B* (1-Y) + C * (1-Y).^2;   % Equation used by the Texas group
        
        % GE
        R2_star_in = R2_star_0in + R2_star_Hb_in; % GE
    case 'IV_R2starRevised'
        % Intrinsic relaxation rates of blood % In this method different hematocrit values of blood vessels are ignored
        R2_0in = 2.74 * B0 - 0.6;                 % SE
        R2star_adjustmentFactor = 4;
        R2_star_0in = R2_0in*R2star_adjustmentFactor; % GE

        % Intravascular R2 as a function of O2 saturation (blood phantom data)
        R2Hb_in = 12.67 * B0^2 *(1-Y).^2;         % SE
        R2_star_Hb_in = R2Hb_in;            % GE
        
        % Total intravascular relaxation
        R2_in = R2_0in + R2Hb_in;                 % SE
%         R2_star_in = R2_in;                       % GE, Following DN's opinion about R2*of blood
        R2_star_in = (R2_star_0in + R2_star_Hb_in);   % GE, experimental measurements of IV contribution
end
%************************************************************************************************************
% TISSUE
%************************************************************************************************************
% Intrinsic relaxation rates of tissue (at Y = 100%)
R2_0ex = 1.74* B0 + 7.77;                            % [1/s] Result of fitting the experimental R2,0 shown in Table 1 in Uludag 2009
R2_star_0ex = 3.74* B0 + 7.77;                       % [1/s], data source for the fitting not reported
% R2_star_0ex = 3.74* B0 + 4.77;                     % [1/s], varied so that it corresponds to the mean signal change observed by PK through the cortex

% Extra-vascular relaxation rate as a function of O2
% saturation(Monte-Carlo), a coefficient in Uludag2009 has been changed
% because it does not correspond to the graphs he shows in the paper
delta_vs = NVC.deltaChi_0 /(4*pi)*NVC.Hct * abs(NVC.Yoff - Y)* MR.gamma* B0;
if (simuPial)
    CBV = min(CBV, 0.05); % The coefficients were calculated up to this volume in Uludag 2009
end
fitCoeff_SE = getHbExFitCoeff ('SE', NVC.nCompartments, simuPial, VM, MR);
R2_Hb_ex = (fitCoeff_SE.a.* delta_vs.^5 + fitCoeff_SE.b.* delta_vs.^4 + fitCoeff_SE.c.* delta_vs.^3 + fitCoeff_SE.d .* delta_vs.^2 + fitCoeff_SE.e.* delta_vs + fitCoeff_SE.f).* CBV * 100; % SE, *100, because the relaxation rate is given per % of CBV, chech why it's not CBV+dCBV  %Atena; I changed the matrix size here. i chose only 6 row ao 10 rows in fit coefficient. Check it later, it might be wrong

fitCoeff_GRE = getHbExFitCoeff ('GRE', NVC.nCompartments, simuPial, VM, MR);% In GE I do not use the outcome of the MonteCarlo simulation
R2_star_Hb_ex = (fitCoeff_GRE.a .* delta_vs.^5 + fitCoeff_GRE.b .* delta_vs.^4 + fitCoeff_GRE.c .* delta_vs.^3 + fitCoeff_GRE.d .* delta_vs.^2 + fitCoeff_GRE.e .* delta_vs + fitCoeff_GRE.f) .* (CBV) * 100; % GRE, *100, because the relaxation rate is given per % of CBV, chech why it's not CBV+dCBV

% TOTAL EV RELAXATION
R2_ex = R2_0ex + sum(R2_Hb_ex);
R2_star_ex = R2_star_0ex + sum(R2_star_Hb_ex);

% Koopmans R2_star values at rest across the cortex
% if(strcmp(MR.EV_R2_mode, 'LayerR2star'))
%     if (strcmp(state, 'rest'))
%         R2_star_ex = getLayerR2star( B0, curLayer);
%         R2_star_ex_0_act = R2_star_ex - sum(R2_star_Hb_ex);
%     else
%         R2_star_ex = R2_star_ex_0_act + sum(R2_star_Hb_ex);
%     end
%     R2_star_0ex = R2_star_ex_0_act;
% end

%% RELAXED SIGNALS
% S_in_SE_BOLD = NVC.Rho_bl * exp(-(R2_in* MR.TE_SE(curB0)));          % SE
% S_ex_SE_BOLD = NVC.Rho_GM * exp(-(R2_ex* MR.TE_SE(curB0)));          % SE

S_in_GRE_BOLD = NVC.Rho_bl * exp(-(R2_star_in* MR.TE_GE(curB0)));    % GE
S_ex_GRE_BOLD = NVC.Rho_GM * exp(-(R2_star_ex* MR.TE_GE(curB0)));    % GE
% [SE;GE], if a second dimension exists, then that refers to the oxygenation value
S_in_BOLD    = S_in_GRE_BOLD;
S_ex_BOLD    = S_ex_GRE_BOLD;
R2_in_sum    = R2_star_in;
R2_ex_sum    = R2_star_ex;
R2_tissue    = R2_star_0ex;
R2_tissue_O2 = R2_star_Hb_ex;
R2_blood     =  R2_star_0in;
R2_blood_O2  = R2_star_Hb_in; 
S_nulled_VASO = [];
S_BC_VASO = [];
end
end
