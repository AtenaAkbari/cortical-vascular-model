%%
% GETHBEXFITCOEFF
%
% Irati Markuerkiaga, 01.10.2013
%
%
% Description: Obtain fitting coefficients for calculating the extravascular relaxation as a
% function of blood oxygen saturation using Monte-Carlo simulation
%
% delta_vs = deltaChi_0 / (4*pi)*Hct * abs(Y_off - Y)* gamma* B0;
% R2_star_Hb_ex = (a * delta_vs^5 + b* delta_vs^4 + c * delta_vs^3 + d * delta_vs^2 + e * delta_vs + f) * CBV;
%
% COMMENTS:
% - When calculating the fitting coefficients the random orientation of the
% vessles and the diffusion of the coeffiecients was aleady considered
%
% - The structure of the coeff Matrix: 
%   - First column: diameter of the vessel
%   - Columns 2-6 are the coefficients of the fit from a to f respectively.
%
% - Vessel diameters were obtained from BoasVascularDistribution.xlsx to devide the capillariy volumes obtained from Koopmans2011 
% into the correct type of vascular comparment. The coefficients for the
% different radii have been interpolated from those given in Table 2 in
% Uludag 2009
%
% The coefficients for 200at90 at GE have been arbitararily changed so that they
% fit the curve in Figure 2 in Uludag. 
%
% TODO
% Modified: 14.01.2014
% Last modified: 29.10.2014: Modified only the header 
%%
function[fitCoeff] = getHbExFitCoeff (SeqType, nCompartments, simuPial, VM, MR)

if strcmp(MR.orientationOfCortex, 'random')
coeffMatrix_SE =[24.5, -1.71e-11, 1.12e-8, -2.57e-6, 2.23e-4,  5.96e-3, -0.0340; % Interpolated between 16 and 60 um. Coefficients are those from 16 x 0.89  
                 12.8, -2.69e-11, 1.76e-8, -4.05e-6, 3.51e-4,  9.38e-3, -0.0535; % Interpolated between 16 and 5 um. Coefficients are those from 16 x 1.4
                    5, -1.13e-11, 0.96e-8, -3.22e-6, 4.90e-4, -3.58e-3, 0.0175;
                 12.8, -2.69e-11, 1.76e-8, -4.05e-6, 3.51e-4,  9.38e-3, -0.0535; % Interpolated between 16 and 5 um. Coefficients are those from 16 x 1.4  
                 24.5, -1.71e-11, 1.12e-8, -2.57e-6, 2.23e-4,  5.96e-3, -0.0340; % Interpolated between 16 and 60 um. Coefficients are those from 16 x 0.89  
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4; % 200@90degrees orientation
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
                  200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4];

coeffMatrix_GRE =[ 24.5,         0,        0,       0, -3.56e-6, 0.0453, -0.194; % this line has been added to coincide with the arteriole oxygenation 
                   12.8,         0,        0,       0, -3.56e-6, 0.0453, -0.194; % this line has been added to coincide with the arteriole oxygenation  
                      5,         0,  5.04e-9, -3.05e-6, 6.17e-4, -8.02e-4, -0.005;
                   12.8,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                   24.6,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    50,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617,-0.0166; % 200@90degrees orientation neuk aldatutako balioak, Uludag-en taulan zenbakiren bat gaizki dago!
                    50,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    50,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166];
                    
%                     200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
%                     200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
%                     200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
%                     200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
%                     200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717,
%                     -0.166]; % 200@90degrees orientation, Uludag
%                     orijinala, baina ez ditu bertako grafikoak
%                     erreproduzitzen
%        
elseif strcmp(MR.orientationOfCortex, 'perpendicular')
    coeffMatrix_SE =[24.5, -1.71e-11, 1.12e-8, -2.57e-6, 2.23e-4,  5.96e-3, -0.0340; % Interpolated between 16 and 60 um. Coefficients are those from 16 x 0.89
        12.8, -2.69e-11, 1.76e-8, -4.05e-6, 3.51e-4,  9.38e-3, -0.0535; % Interpolated between 16 and 5 um. Coefficients are those from 16 x 1.4
        5, -1.13e-11, 0.96e-8, -3.22e-6, 4.90e-4, -3.58e-3, 0.0175;
        12.8, -2.69e-11, 1.76e-8, -4.05e-6, 3.51e-4,  9.38e-3, -0.0535; % Interpolated between 16 and 5 um. Coefficients are those from 16 x 1.4
        24.5, -1.71e-11, 1.12e-8, -2.57e-6, 2.23e-4,  5.96e-3, -0.0340; % Interpolated between 16 and 60 um. Coefficients are those from 16 x 0.89
        200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
        200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
        200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
        200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4;
        200,           0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4]; % 200@90degrees orientation

coeffMatrix_GRE =[ 24.5,         0,        0,       0, -3.56e-6, 0.0453, -0.194; % this line has been added to coincide with the arteriole oxygenation 
                   12.8,         0,        0,       0, -3.56e-6, 0.0453, -0.194; % this line has been added to coincide with the arteriole oxygenation  
                      5,         0,  5.04e-9, -3.05e-6, 6.17e-4, -8.02e-4, -0.005;
                   12.8,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                   24.6,         0,        0,       0, -3.56e-6, 0.0453, -0.194; 
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617,-0.0166; % 200@90degrees orientation neuk aldatutako balioak, Uludag-en taulan zenbakiren bat gaizki dago!
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166;
                    
                    200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
                    200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
                    200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
                    200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166;
                    200,         0, -8.76e-10, 7.29e-6, -3.42e-5, 0.0717, -0.166]; % 200@90degrees orientation, Uludag
%                     orijinala, baina ez ditu bertako grafikoak
%                     erreproduzitzen
  
else
    disp('The variable MR.orientationOfCortex does not have a valid value')
    return
end
    
if (~simuPial)
    coeffMatrix_SE = coeffMatrix_SE ((5*VM.propagation+1: 5*VM.propagation+nCompartments), :);
    coeffMatrix_GRE = coeffMatrix_GRE ((5*VM.propagation+1: 5*VM.propagation+nCompartments), :);
else
    coeffMatrix_SE =  [200,         0, 1.47e-10, -9.77e-8, 2.41e-5, -1.10e-4, 4.91e-4];
    if (strcmp(VM.PialOrientation, 'perpendicular'))
        coeffMatrix_GRE = [200,         0, -8.76e-11, 7.29e-7, -3.42e-6, 0.0617, -0.0166];
    else
        coeffMatrix_GRE = [200,         0,        0,       0, -3.56e-6, 0.0453, -0.194]; 
    end
end


if (strcmp(SeqType, 'SE'))
    fitCoeff.a = coeffMatrix_SE(:, 2);
    fitCoeff.b = coeffMatrix_SE(:, 3);
    fitCoeff.c = coeffMatrix_SE(:, 4);
    fitCoeff.d = coeffMatrix_SE(:, 5);
    fitCoeff.e = coeffMatrix_SE(:, 6);
    fitCoeff.f = coeffMatrix_SE(:, 7);
else    
    fitCoeff.a = coeffMatrix_GRE(:, 2);
    fitCoeff.b = coeffMatrix_GRE(:, 3);
    fitCoeff.c = coeffMatrix_GRE(:, 4);
    fitCoeff.d = coeffMatrix_GRE(:, 5);
    fitCoeff.e = coeffMatrix_GRE(:, 6);
    fitCoeff.f = coeffMatrix_GRE(:, 7);
end
    
 end
    
