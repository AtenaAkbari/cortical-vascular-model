%%
% GETBLOODOXYGENCOEFF_GRE.m
%
% Irati Markuerkiaga, 22.10.2013
%
% Description: Function to get the constant value C required for computing the
% intravascular relaxation rate depending on the oxygenation value Y for GRE: 
% R2_star_Hb_in = C * (1-Y)^2
%
% NOTE: C values for B0> 4.7 T have been set to an arbitrarily high value
% to null the signal in them.
%
% Last modified: 29.10.2014

%%
function [R2_star_Hb_in] = getBloodOxygenCoeff_GE (fieldStrength)

B0 = [1.5 3 4 4.7 7 9.4 11.7 14 16.1];                            % Magnetic field [T]

if fieldStrength > 4.7
    R2_star_Hb_in = (319-25)/(4.7-1.5)*fieldStrength -112.8125;
else
    R2_star_Hb_in_array = [25 181 262 319 1e6 1e6 1e6 1e6 1e6];       % Coefficients for Blood Relaxation values depending on the oxygenation level for GRE
    R2_star_Hb_in = R2_star_Hb_in_array(B0 == fieldStrength);
end