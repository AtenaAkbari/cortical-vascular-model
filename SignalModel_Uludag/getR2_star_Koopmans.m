%%
% GETT2_STAR_KOOPMANS
%
% Author: Irati Markuerkiaga
% 
% Description: I read T2* values from the graph in his multiecho paper
% (ref!). This function return R2* for that layer
% 
% NOTES:
%
% TODO:
%
% Last modified: 02.11.2014

%%
function [R2_star_rest] = getR2_star_Koopmans(m)

T2_star_koopmans2011 = [26 27.2 28 27.6 27.6 28.2 30 31.7 31.7 30 28];
R2_star_rest = 1/T2_star_koopmans2011(m)*1000;

end