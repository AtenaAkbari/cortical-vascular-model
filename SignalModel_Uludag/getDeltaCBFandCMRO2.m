%%
% GETDELTACBFANDCMRO2
%
% Irati Markuerkiaga
% Description: Calculates the oxygenation and CBF values upon activation based on the relative strength of the activation
%
% Relationship between CMRO2, CBF and Y (Kim 1999)
% deltaCMRO2/CMRO2 = (deltaCBF/CBF+1)*(1-(deltaY/(Y_off-Y))) - 1
%
% According to Buxton 2014, typical values for strong activation in the visual cortex:
%   deltaCBF = 40%
%   n = 2 -> deltaCMRO2 = 20%
%   alpha_v = 0.2
% Note: The BOLD signal depends strongly on the coupling ratio n, the ratio
% between the fractional changes in CBF and CMRO2, CAREFUL!!: n changes
% depending on brain region, task, disease...
%
% The stimulus strength-CBF transfer function was taken from Liang 2013, 
% Table 1. Visual stimulation. 1036cd/m2 is the maximum luminance observable by the eye

% TODO: 
% - Find also, if possible, transfer function between activation_strength
% and deltaCMRO2 -> we know n changes, but depending on what?!
%
% 2014.11.11: Assumptions introduced:
%               1) CBF changes following the power low with stimulus
%               strength obtained from Liang2013: deltaCBF= 0.58 *
%               relStrength^0.18
%               2) deltaCMRO2 = deltaCBF/n
% 2015.01.28: Griffeth2014 added
%
% Last modified: 28.01.2015
%%
function [r_deltaCBF ,Y_act_vein,x_act]= getDeltaCBFandCMRO2(NVC, curLayer)

if (strcmp(NVC.dilationMode ,'Uludag2009'))
    if (NVC.relStrength(curLayer) ==1) % For single layer activation only, not different relStrengths across the cortex
        r_deltaCBF = .5;
        Y_act_vein = 0.78;
        x_act      = 1;
    
    else
        r_deltaCBF = .5;
        Y_act_vein = 0.78;
        x_act      = 1;
    end
    
else
    % CBF INCREASE UPON ACTIVATION
    if(strcmp(NVC.stimCBF_TF ,'linear'))
        r_deltaCBF = NVC.deltaCBFmax * NVC.relStrength(curLayer); % Not the best TF, it is known that CBF saturates quite early on
    else
        if(strcmp(NVC.stimCBF_TF ,'Griffeth2014'))
            if (NVC.relStrength(curLayer) == 1) &&  any((NVC.relStrength ~=1))  % Same as Uludag, 'max' activation not in Griffeth 2014
                r_deltaCBF = 0.5;
            else
            % Following Griffeth2014, Results written text
            r_deltaCBF =  NVC.Griffeth2014.cbf(NVC.Griffeth2014.stim);
            end
        else
            % Following Liang2013, Table 1
%             r_deltaCBF = 0.58 * NVC.relStrength(curLayer)^0.18; % Taken from Liang 2013, Table 1. 1036cd/m2 is the maximum luminance observable by the eye
%             r_deltaCBF = 0.50 * NVC.relStrength(curLayer)^0.18; % Taken from Liang 2013, Table 1. 1036cd/m2 is the maximum luminance observable by the eye
            r_deltaCBF =  NVC.deltaCBFmax  * NVC.relStrength(curLayer)^0.18; % Taken from Liang 2013, Table 1. 1036cd/m2 is the maximum luminance observable by the eye
        end
    end
    
    % CMRO2 INCREASE UPON ACTIVATION
    if (strcmp(NVC.stimCBF_TF ,'linear'))||(strcmp(NVC.stimCBF_TF ,'Liang2013'))  % Following Buxton 2014, Blockley
        r_deltaCMRO2 = r_deltaCBF/NVC.n;
    else
        if(NVC.relStrength(curLayer) == 1) &&  any((NVC.relStrength ~=1)) % Same as Uludag, 'max' activation not in Griffeth 2014
            r_deltaCMRO2 = r_deltaCBF /NVC.n;
        else
            r_deltaCMRO2 = NVC.Griffeth2014.cmro2(NVC.Griffeth2014.stim);
        end
    end
    
    % OXYGENATION IN THE DIFFERENT VASCULAR COMPARTMENTS UPON ACTIVATION
    
    % Full oxygenation of arterioles reached when r_deltaCBF == 0.1,
    % arbitrarily set because I don't know what is the extraction fraction from
    % the capillaries, but it should be a low value because at 10% stim
    % intensity there is a 20% CBF increase (Griffeth2015)
    % Arterioles:
    x_act = min(NVC.x_act, (NVC.Yoff + 50 * r_deltaCBF)); 
    
    
    % Venules
    % Y increase upon activation (Following Kim1999)
    deltaY     = (x_act - NVC.oxygBas_cte) * ( 1- ((r_deltaCMRO2 + 1)/(r_deltaCBF + 1)));
    Y_act_vein = NVC.oxygBas_cte + deltaY;
    
   % Limit Y_act to compare results with Uludag2009, but always check this
   % value with new settings
    if Y_act_vein > NVC.oxygAct_cte
        Y_act_vein = NVC.oxygAct_cte;
    end
end
end
