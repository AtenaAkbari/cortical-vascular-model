%%
% GETLAYERR2STAR
%
% Author: Irati Markuerkiaga
% 
% Description: Koopmans 2011 measured layer T2* values at 7T. This code
% interpolated these values to other field strengths simulated in
% Uludag2009. 
% 
% ASSUMPTIONS:
% - R2 star values scale linearly with field strength. Reasoning behind: In
% Table 2 in Uludag 2009, coefficient e is at least one order of magnitude
% than the rest of the coefficients. Therefore, R2*Hbex changes quite
% linearly with delta_vs, which is proportional to B0. -> Difference in R2*
% between Koopmans and Uludag at 7T will be scaled with B0.
%
% - I assume that the differences between Koopmans2011 and Uludag2009 are
% due to susceptibility differences between layers, which are mainly due to
% diffences in myelin content.
%
% NOTES:
% -
%
% TODO:
% - Check with David if the interpolation is correct.
%
%
% Last modified: 03.11.2014
%%
function R2_star = getLayerR2star( B0, nLayer)

% Obtain layer R2* as given in Koopmans2011 (The multi-echo paper)
R2_starAt7T = getR2_star_Koopmans(nLayer);
% R2_starAt7T = [34.4784   32.4964   31.3042   30.4584   30.4219   29.7708    27.4337   25.2730   24.8788   26.1102]; % Balio hauek ez dakit nondik atera ditudan!!!!!

% % ALFERRIK KONPLIKATZEA DELA USTE DUT!:
% % The change in R2* is believed to scale with B0 linearly
% R2_star7T_Uludag = 3.74*7+9.77;
% R2_starRef_Uludag = 3.74*B0+9.77;
% 
% R2adjustFactor = (R2_starAt7T - R2_star7T_Uludag)/R2_star7T_Uludag;
% 
% % Return interpolated R2* value for that layer at that field strength
% R2_star = R2_starRef_Uludag*( 1+ R2adjustFactor);

% Era sinpleagoa, B0-rekin linealki aldatu!
R2_star = R2_starAt7T * B0/7; % 2015.04.27
