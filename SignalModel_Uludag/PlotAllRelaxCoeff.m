%%
% Irati Markuerkiaga, 22.10.2013

% Script to plot Figures 1 and 2 of the Uludag2009 paper. 
% Figure 1: Intrinsic tissue relaxation rate as a function on B0.
%           Intrinsic blood relaxation rate as a function on B0.
%           Blood relaxation rate for SE for different oxygenation values as a function on B0.

% Figure 2: Tissue relaxation rate for different cylinder diameters for GRE as a function on B0.
%           Tissue relaxation rate for different cylinder diameters for SE as a function on B0.
%           The oxygenation level of each vascular compartment is fixed in
%           getAct_vol_oxy.m
            
% Last modified: 22.10.2013

%% Plots
% getRelaxedS-en irteerarako egokitzeke
figure (1)
B0 = 7;
subplot(3,1,1)
plot(B0,R2_tissue_array(1,:), 'o--k')
hold on, grid on
plot(B0, R2_tissue_array(2,:), '*--r')
title('Intrinsic tissue relaxation rate')
axis([0 10 0 50])
legend ('R2', 'R2*','location', 'SouthEast')
xlabel('B_0 [T]'), ylabel('Relaxation rate [1/s]')

subplot(3,1,2)
plot(B0, R2_blood_array(1,:), 'o--k')
hold on, grid on
plot(B0, R2_blood_array(2,:), '*--r')
title('Intrinsic blood relaxation rate')
axis([0 10 0 50])
legend('R2', 'R2*', 'location', 'SouthEast')
xlabel('B_0 [T]'), ylabel('Relaxation rate [1/s]')

subplot(3,1,3)
plot(B0, squeeze(R2_blood_O2_array(1,1,:)), 'o--r')
hold on, grid on
plot(B0, squeeze(R2_blood_O2_array(1,2,:)), '*--b')
hold on
plot(B0, squeeze(R2_blood_O2_array(1,3,:)), 'x--g')
title('SE blood total relaxation rate')
axis([0 10 0 200])
legend(strcat(num2str(Act_vol_oxy_all(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(3,1)), ' um /', num2str(Act_vol_oxy(3,4)), ' O2sat'), 'location', 'NorthEast')
xlabel('B_0 [T]'), ylabel('Relaxation rate [1/s]')

figure (2)
subplot(2,1,2)
plot(B0, squeeze(R2_tissue_O2_array(1,1,:)), 'o--r')
hold on, grid on
plot(B0, squeeze(R2_tissue_O2_array(1,2,:)), '*--k')
hold on
plot(B0, squeeze(R2_tissue_O2_array(1,3,:)), 'x--g')
hold on
plot(B0, squeeze(R2_tissue_O2_array(1,4,:)), 'x--m')
title('Tissue relaxation rate for each vessel type, SE')
axis([0 17 -1 7])
xlabel('B_0 [T]'), ylabel('Relaxation rate [1/s]')
% legend(strcat(num2str(Act_vol_oxy(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), 'location', 'NorthEastOutside')
% legend(strcat(num2str(Act_vol_oxy(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(3,1)), ' um /', num2str(Act_vol_oxy(3,4)), ' O2sat'), 'location', 'NorthEastOutside')
legend(strcat(num2str(Act_vol_oxy_all(1,1)), ' um /', num2str(Act_vol_oxy_all(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy_all(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(3,1)), ' um /', num2str(Act_vol_oxy(3,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(4,1)), ' um /', num2str(Act_vol_oxy(4,4)), ' O2sat'), 'location', 'NorthEastOutside')

subplot(2,1,1)
plot(B0, squeeze(R2_tissue_O2_array(2,1,:)), 'o--r')
hold on, grid on
plot(B0, squeeze(R2_tissue_O2_array(2,2,:)), '*--k')
hold on
plot(B0, squeeze(R2_tissue_O2_array(2,3,:)), 'x--g')
hold on
plot(B0, squeeze(R2_tissue_O2_array(2,4,:)), 'x--m')
title('Tissue relaxation rate for each vessel type, GRE')
axis([0 17 -1 20])
xlabel('B_0 [T]'), ylabel('Relaxation rate [1/s]')
% legend(strcat(num2str(Act_vol_oxy(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), 'location', 'NorthEastOutside')
% legend(strcat(num2str(Act_vol_oxy(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(3,1)), ' um /', num2str(Act_vol_oxy(3,4)), ' O2sat'), 'location', 'NorthEastOutside')
legend(strcat(num2str(Act_vol_oxy(1,1)), ' um /', num2str(Act_vol_oxy(1,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(2,1)), ' um /', num2str(Act_vol_oxy(2,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(3,1)), ' um /', num2str(Act_vol_oxy(3,4)), ' O2sat'), strcat(num2str(Act_vol_oxy(4,1)), ' um /', num2str(Act_vol_oxy(4,4)), ' O2sat'), 'location', 'NorthEastOutside')
