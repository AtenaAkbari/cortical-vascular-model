%%
% GETBLOODOXYGENCOEFF_GRE.m
%
% Irati Markuerkiaga, 22.10.2013
%
% Description: Function to get the constant value C required for computing the
% intravascular relaxation rate depending on the oxygenation value Y for GE: 
% R2_star_Hb_in = C * (1-Y)^2
%
% NOTE: 
% For B0> 4.7T, C has been extrapolated by applying:
%      C = 91.817*B0 -106.24
% (See fitting in Extrapol_R2star.xlsx)
%
%
% Last modified: 29.10.2014

%%
function [BloodOxygenCoeff_GE] = getBloodOxygenCoeff_GE (fieldStrength)

B0_local = [1.5 3 4 4.7 7 9.4 11.7 14 16.1];                                % Magnetic field[T]

if fieldStrength > 4.7
    BloodOxygenCoeff_GE = 91.817*fieldStrength -106.24;
else
    BloodOxygenCoeff_GE_array = [25 181 262 319 1e6 1e6 1e6 1e6 1e6];         % Coefficients for Blood Relaxation values depending on the oxygenation levelfor GRE
    BloodOxygenCoeff_GE = BloodOxygenCoeff_GE_array(B0_local == fieldStrength);
end