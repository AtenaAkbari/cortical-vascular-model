%% 
% GETLUCOEFFS
%
% Author: Irati Markuerkiaga, 2014.11.13
%
% Function that returns the coefficients for calculating Y and Hct
% dependent T2 times of blood at 3T as reported by Lu2012
%
% These coefficients completely characterize the relationship between blood
% T2, Y, and Hct at a specific tCPMG. 
%                    a1 (s^-1)  a2 (s^-1) a3 (s^-1) b1 (s^-1) b2 (s^-1) c1 (s^-1) 
% tCPMG � 5ms
% tCPMG � 10 ms
% tCPMG � 15 ms
% tCPMG � 20 ms
%
% Last changed: 2014.11.13
%%
function [arOut] =  getLuCoeffs(tau)

Lu_coeffs = [
-4.4 39.1 -33.5 1.5 4.7 167.8; 
-13.5 80.2 -75.9 -0.5 3.4 247.4;
-12.0 77.7 -75.5 -6.6 31.4 249.4;
7.0 -9.2 23.2 -4.5 5.3 310.8];

arOut = Lu_coeffs(tau,:);