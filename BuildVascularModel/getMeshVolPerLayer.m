%
% GETMESHVOLPERLAYER.m
%
% Author: Irati Markuerkiaga, 17.04.2014
%
% Description: The number of datapoints across the cortex obtained from
% experimental data may not correspond to the number of voxels in our
% simulation. This code interpolates the mesh volume density to the number
% of voxels that are being simulated.
%
% NOTE:
%
% TODO
%
% Last modified: 30.10.2014
%%
function meshVolumeFraction = getMeshVolPerLayer (cortLayersBoundaries, meshFraction_orig, meshVolumeFraction, voxelCortR)

rep_scale = 1000;
rep = cortLayersBoundaries(:,2)*rep_scale;
C = arrayfun(@(x, y) repmat(x, [1 y]), meshFraction_orig,rep', 'UniformOutput', false);
meshFraction_orig_Steps = cell2mat(C);                                         % [L4 L3 L2 L1]
l = length(meshFraction_orig_Steps);

for i = 1 : length(meshVolumeFraction)
    lower = round(voxelCortR*(i-1)*l)+1;
    upper = round(voxelCortR*(i)*l);
    meshVolumeFraction(i) = (sum(meshFraction_orig_Steps(lower : upper)))/((upper-lower)); % [L1 L2 L3 L4]
end
meshVolumeFraction = fliplr(meshVolumeFraction); % WM -> CSF
end