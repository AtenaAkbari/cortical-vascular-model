%%
% BUILDVASCULARMODEL_1XG4G3
%
% Author: Irati Markuerkiaga, 26.05.2014
% ICA addition : Atena 
%
% Description: Build the vascular model as one of the two vascular units
% described by Duvernoy1981, namely: 2 smaller veins surrounded by several
% arteries. This model does not have a G5 (i.e. big vessel that drains both
% WM and GM). Instead it has one G4 and one G3 intracortical vein. (naming
% follows Duvernoy 1981). In the second part of the code it calculates the
% oxygenation values through the ICVs upon activation if the activation
% strength varies throught the cortex. This is important for simulating the
% BOLD signal at a later stage.
%
%% 
% ASSUMPTIONS/SIMPLIFICATIONS
% - The draining flow is equally distributed among all ICVs present in
%   the layer
% - All ICVs drain all layers they are present in.
% - Combining Boas2008 and Lauwers2008 (capillaries diam <10um) we get that
%   the distribution in the capillary mesh is as follows: arterioles 21%, 
%   capillaries 36% and Venules 43%
% - The macroVasc volume is the ICV volume + the volume of the vessels
%   bigger than 20um. This was computed by combining Weber2008 + the volume
%   ratio for these type of vessels in the Boas2008 branching unit.
% - Velocities in ICV are computed with the regression curve I obtained
%   from Lipowsky's data, whereas I took Boas data for the microvascular mesh
% 
%
% NOTE:
% 1- volume fractions are per area, that is why it is weighted with voxel
% side to get the number of volume branchings.
% 2- ICV diameters are calculated for each voxel! 
%
% TODO
% - Correct the venous volume according to the number of venules smaller than
% the biggest venule defined in Boas2008 that drain into ICVs. I don't
% think this will make a big difference
%
% 30.10.2014: Code cleaned
% 10.12.2014: I modified it such that the vascular tree in the Boas model is not necessary and
% only the capillaries count
% 27.05.2015: I modified the way of calculating the diameter of G4 so that
% the rest of the ICVs drain all layers and the diameters are still within
% a reasonable range.
% 29.06.2015: Changed code so that flow is equally distributed among all
% vessels present in a layer.
% 07.08.2015: Final Cleaning
%
% Last modified: 07.08.2015
%% 
function VMout = buildVascularModel_1xG4G3_2xA4A3(VM, NVC, VR)
%% VASCULAR MODEL OF THE CORTEX
% DEFINE PARAMETERS
voxelCortR     = VM.lp.voxelLength/VM.lp.corticalThickness;                 % Voxel to cortical thickness ratio (direction perpendicular to cortical surface)
nrBigVes_vein  = VM.lp.nrBigVes_vein;                                       % Berrikusi hau, buklean balio aldatu diot eta horregatik dago hemen
nrBigVes_art   = VM.lp.nrBigVes_art;
bigVes_Ar_vein = 1 : nrBigVes_vein;
bigVes_Ar_art  = 1 : nrBigVes_art;                                          % Indices to fill the array correctly

    
% INITIALIZATION     
veinOxSat_act = zeros (VM.nLayers,VM.lp.newVessLayer_vein, VM.nICV); 
artOxSat_act  = zeros (VM.nLayers,VM.lp.newVessLayer_art, VM.nICA); 
veinDiam      = zeros(VM.nLayers,VM.lp.newVessLayer_vein, VM.nICV);
artDiam       = zeros(VM.nLayers,VM.lp.newVessLayer_art, VM.nICA);          % Diameter of ICVs
veinVel       = zeros(VM.nLayers, VM.lp.newVessLayer_vein, VM.nICV);
artVel        = zeros(VM.nLayers, VM.lp.newVessLayer_art, VM.nICA);         % Velocity in the ICVs 
P_vein        = zeros(VM.nLayers, VM.lp.newVessLayer_vein, VM.nICV);
P_art         = zeros(VM.nLayers, VM.lp.newVessLayer_art, VM.nICA);         % Mass flux, p = r^2*v
P_in_ICV      = zeros (1,VM.nLayers);
P_in_ICA      = zeros (1,VM.nLayers);                                       % Incoming P to each ICV in a layer
meshVolumeFraction = zeros(1, VM.nLayers);                                  % Mesh volume fraction across all the simulated layer/voxels        

%% CODE
%% CALCULATE ICV DIAMETERS
% Mass quantitiy, P, for the venules given the diameters as defined in Boas 2008
% and velocities in Lipowsky 2005 (based on Zweifach 1974)
P_cap = NVC.diam_cap^3 /4 * VM.lp.alpha_cap;   % P through a single capillary
% Get the mesh volume (in %) for every voxel
meshVolumeFraction = getMeshVolPerLayer (VM.lp.cortLayersBoundaries, VM.lp.meshFraction_orig, meshVolumeFraction, voxelCortR);

% Calculate the number of capillaries in each voxel
capVol   = meshVolumeFraction * NVC.capFrac * VM.lp.voxelVol;               
getNrcap = capVol./(pi*(NVC.diam_cap/2)^2*NVC.l_cap);                       


% Flip the variables to calculate the size of the ICVs
nICV_array      = fliplr(VM.lp.nICV_array);
nICA_array      = fliplr(VM.lp.nICA_array);
numVessels_vein = fliplr(VM.lp.numVesselLayer_vein);
numVessels_art  = fliplr(VM.lp.numVesselLayer_art);

%% CALCULATE DIAMETERS OF ICVs

% Calculate diameters for the rest of the ICVs
for layerIdx = 1 : VM.nLayers
    P_in_ICV(layerIdx) = (getNrcap(layerIdx)/ numVessels_vein(layerIdx))*P_cap;
    if layerIdx == 1     % If first layers (i.e. closest to WM)
        veinDiam(layerIdx,bigVes_Ar_vein,1) = ((4/VM.lp.alpha_post_cap) * P_in_ICV(layerIdx)).^(1/3);
        veinVel(layerIdx,bigVes_Ar_vein,1)  = VM.lp.alpha_post_cap * veinDiam(layerIdx,bigVes_Ar_vein,1);
    else
        for icvIdx = 1 : nICV_array(layerIdx)
            if (icvIdx == 1) || (icvIdx == 2)   % If only one vessel of this kind
                veinDiam(layerIdx,bigVes_Ar_vein, icvIdx) = ((P_in_ICV(layerIdx) + P_vein(layerIdx-1,1,icvIdx))*4/ VM.lp.alpha_post_cap).^(1/3);
            else
                veinDiam(layerIdx,:, icvIdx) = ((P_in_ICV(layerIdx) + P_vein(layerIdx-1,:,icvIdx))*4/ VM.lp.alpha_post_cap).^(1/3);
            end
            veinVel(layerIdx,:, icvIdx) =  VM.lp.alpha_post_cap * veinDiam(layerIdx,:, icvIdx);
        end
    end
    P_vein(layerIdx,:,:) = (veinDiam(layerIdx,:,:)/2).^2.* veinVel(layerIdx,:,:);
end
%% CALCULATE DIAMETERS OF ICAs

for layerIdx = 1 : VM.nLayers
    P_in_ICA(layerIdx) = (getNrcap(layerIdx)/ numVessels_art(layerIdx))*P_cap;
    if layerIdx == 1   % If first layers (i.e. closest to WM)
        for icaIdx = 1 : nICA_array(layerIdx)
        artDiam(layerIdx,bigVes_Ar_art,icaIdx) = ((4/VM.lp.alpha_pre_cap) * P_in_ICA(layerIdx)).^(1/3);
        artVel(layerIdx,bigVes_Ar_art,icaIdx)  = VM.lp.alpha_pre_cap * artDiam(layerIdx,bigVes_Ar_art,1);
        end
    else
        for icaIdx = 1 : nICA_array(layerIdx)
            if (icaIdx == 1) || (icaIdx == 2) % If only one vessel of this kind
                artDiam(layerIdx,bigVes_Ar_art, icaIdx) = ((P_in_ICA(layerIdx) + P_art(layerIdx-1,1,icaIdx))*4/ VM.lp.alpha_pre_cap).^(1/3);
            else
                artDiam(layerIdx,:, icaIdx) = ((P_in_ICA(layerIdx) + P_art(layerIdx-1,:,icaIdx))*4/ VM.lp.alpha_pre_cap).^(1/3);
            end
            artVel(layerIdx,:, icaIdx) =  VM.lp.alpha_pre_cap * artDiam(layerIdx,:, icaIdx);
        end
    end
    P_art(layerIdx,:,:) = (artDiam(layerIdx,:,:)/2).^2.* artVel(layerIdx,:,:);
end

% for layerIdx = VM.nLayers:-1:1
%     P_in_ICA(layerIdx) = (getNrcap(layerIdx)/ numVessels_art(layerIdx))*P_cap;
%     if layerIdx == 10   % closest to surface
%         for icaIdx = 1 : (nICA_array(layerIdx))
%         artDiam(layerIdx,bigVes_Ar_art,icaIdx) = ((4/VM.lp.alpha_pre_cap) * P_in_ICA(layerIdx)).^(1/3);
%         artVel(layerIdx,bigVes_Ar_art,icaIdx)  = VM.lp.alpha_pre_cap * artDiam(layerIdx,bigVes_Ar_art,1);
%         end
%     else
%         for icaIdx = 1 : nICA_array(layerIdx)
%             if (icaIdx == 1) || (icaIdx == 2) % If only one vessel of this kind
%                 artDiam(layerIdx,bigVes_Ar_art, icaIdx) = ((P_in_ICA(layerIdx) - P_art(layerIdx+1,1,icaIdx))*4/ VM.lp.alpha_pre_cap).^(1/3);
%             else
%                 artDiam(layerIdx,:, icaIdx) = ((P_in_ICA(layerIdx) - P_art(layerIdx+1,:,icaIdx))*4/ VM.lp.alpha_pre_cap).^(1/3);
%             end
%             artVel(layerIdx,:, icaIdx) =  VM.lp.alpha_pre_cap * artDiam(layerIdx,:, icaIdx);
%         end
%     end
%     P_art(layerIdx,:,:) = (artDiam(layerIdx,:,:)/2).^2.* artVel(layerIdx,:,:);
% end


%% CALCULATE OXYGEN SATURATION UPON ACTIVATION IN THE ICVs
P_act_vein = P_vein;

% Adjust oxygenation values obtained if different activation strengths,
% and therefore flows, between layers are simulated
for layerIdx = 1 : VM.nLayers % For all layers
    
    % Obtain flow increase and deltaY in the layer (veins)
    [deltaCBF, Y_act_vein]= getDeltaCBFandCMRO2(NVC, layerIdx);    
    for icvIdx = 1 : nICV_array(layerIdx) % For all ICVs in the layer
        if (layerIdx==1) && (icvIdx==1)
            P_act_vein(layerIdx,:,:) = P_vein(layerIdx,:,:) * (1 + deltaCBF);       % In the voxel located closes to WM, changes in the ICV are the same as in the laminar networks, as there is only one ICV and emerges from this location
            veinOxSat_act(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx) = Y_act_vein;
        else
            if (nICV_array(layerIdx)~= nICV_array(layerIdx-1)) && (icvIdx == nICV_array(layerIdx-1) + 1) % agar faghat yek rag ezafe shode bood % If there is a new group of vessels in the voxel and the current vessel is the new vessel in the voxel
                P_act_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)    = P_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)* (1 +deltaCBF);
                veinOxSat_act(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx) = Y_act_vein;
            else                             
                P_act_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)    = P_act_vein(layerIdx-1,1:VM.lp.idxICVtype(icvIdx),icvIdx) +  P_in_ICV(layerIdx) * (1 + deltaCBF);
                veinOxSat_act(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx) = (P_act_vein(layerIdx-1,1:VM.lp.idxICVtype(icvIdx),icvIdx)./P_act_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)).* veinOxSat_act(layerIdx-1,1:VM.lp.idxICVtype(icvIdx),icvIdx) + ((P_act_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)-P_act_vein(layerIdx-1,1:VM.lp.idxICVtype(icvIdx),icvIdx))./P_act_vein(layerIdx,1:VM.lp.idxICVtype(icvIdx),icvIdx)) * Y_act_vein; % Simultaneous activation in all layers, Comment out this line for computing the spillage of one layer to the layers downstream with getPSF.m
            end
        end
    end
end
f_ICV_suc = pi*(veinDiam/2).^2/ VM.lp.voxelSide^2; % Volume of ICV in a layer


%% CALCULATE OXYGEN SATURATION UPON ACTIVATION IN THE ICAs
% Obtain flow increase and deltaY in the layer (arteries)
P_act_art = P_art;
Y_act_art = 1;
deltaCBF = 0.5;
for layerIdx = 1 : VM.nLayers % For all layers
    
%     [deltaCBF, ~]= getDeltaCBFandCMRO2(NVC, layerIdx);    
    for icaIdx = 1 : nICA_array(layerIdx) % For all ICAs in the layer
        if (layerIdx==1)
            P_act_art(layerIdx,:,:) = P_art(layerIdx,:,:) * (1 + deltaCBF); % In the voxel located closes to WM, changes in the ICV are the same as in the laminar networks, as there is only one ICV and emerges from this location
            artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = Y_act_art;
        else
            if (nICA_array(layerIdx)~= nICA_array(layerIdx-1)) && (icaIdx == nICA_array(layerIdx-1) + 1) % If there is a new group of vessels in the voxel and the current vessel is the new vessel in the voxel
                P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)    = P_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)* (1 +deltaCBF);
                artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = Y_act_art;

            else                             
                P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)    = P_act_art(layerIdx-1,1:VM.lp.idxICAtype(icaIdx),icaIdx) +  P_in_ICA(layerIdx) * (1 + deltaCBF);
                artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = (P_act_art(layerIdx-1,1:VM.lp.idxICAtype(icaIdx),icaIdx)./P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)).* artOxSat_act(layerIdx-1,1:VM.lp.idxICAtype(icaIdx),icaIdx) + ((P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)-P_act_art(layerIdx-1,1:VM.lp.idxICAtype(icaIdx),icaIdx))./P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)) * Y_act_art; % Simultaneous activation in all layers, Comment out this line for computing the spillage of one layer to the layers downstream with getPSF.m

            end
        end
    end
end
% for layerIdx = VM.nLayers:-1:1 % For all layers
%     
% %     [deltaCBF, ~]= getDeltaCBFandCMRO2(NVC, layerIdx);    
%     for icaIdx = 1 : nICA_array(layerIdx) % For all ICAs in the layer
%         if (layerIdx==10)
%             P_act_art(layerIdx,:,:) = P_art(layerIdx,:,:) * (1 + deltaCBF); % In the voxel located closes to WM, changes in the ICV are the same as in the laminar networks, as there is only one ICV and emerges from this location
%             artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = Y_act_art;
%         else
%             if (nICA_array(layerIdx)~= nICA_array(layerIdx+1)) && (icaIdx == nICA_array(layerIdx+1) + 1) % If there is a new group of vessels in the voxel and the current vessel is the new vessel in the voxel
%                 P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)    = P_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)* (1 +deltaCBF);
%                 artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = Y_act_art;
% 
%             else                             
%                 P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)    = P_act_art(layerIdx+1,1:VM.lp.idxICAtype(icaIdx),icaIdx) -  P_in_ICA(layerIdx) * (1 + deltaCBF);
%                 artOxSat_act(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx) = (P_act_art(layerIdx+1,1:VM.lp.idxICAtype(icaIdx),icaIdx)./P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)).* artOxSat_act(layerIdx+1,1:VM.lp.idxICAtype(icaIdx),icaIdx) - ((P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)+P_act_art(layerIdx+1,1:VM.lp.idxICAtype(icaIdx),icaIdx))./P_act_art(layerIdx,1:VM.lp.idxICAtype(icaIdx),icaIdx)) * Y_act_art; % Simultaneous activation in all layers, Comment out this line for computing the spillage of one layer to the layers downstream with getPSF.m
% 
%             end
%         end
%     end
% end
f_ICA_suc = pi*(artDiam/2).^2/ VM.lp.voxelSide^2; % Volume of ICA in a layer
%%  RETURN VARIABLES
VMout.meshVolumeFraction = meshVolumeFraction;
VMout.veinDiam_1         = squeeze(veinDiam(:,1,:))';
VMout.artDiam_1          = squeeze(artDiam(:,1,:))';
VMout.veinVel_1          = squeeze(veinVel(:,1,:))';
VMout.artVel_1           = squeeze(artVel(:,1,:))';
VMout.f_ICV              = squeeze(sum(f_ICV_suc,2))';                            
VMout.f_ICA              = squeeze(sum(f_ICA_suc,2))';                            
VMout.veinOxSat_act      = squeeze(veinOxSat_act(:,1,:))';                        % Vessels of the same category are equal in a layer in this simulation, so the second dimension of matrix veinOxSat_act does not add new information
VMout.artOxSat_act       = squeeze(artOxSat_act(:,1,:))';
VMout.P_vein             = squeeze(P_vein(:,1,:))';
VMout.P_art              = squeeze(P_art(:,1,:))';
VMout.P_act_vein         = squeeze(P_act_vein(:,1,:))';
VMout.P_act_art          = squeeze(P_act_art(:,1,:))';
VMout.model              = '1xG4G3_2xA4A3';

%% VISUALIZE AND SAVE RESULTS
vol_ICV_0 = sum(VMout.f_ICV, 1);
vol_ICA_0 = sum(VMout.f_ICA, 1);
ax1 =linspace(10,90,10); 
plot(ax1,(vol_ICV_0 + vol_ICA_0+ meshVolumeFraction)*100,  'LineWidth',3, 'color', [0.8500, 0.3250, 0.0980]);
hold on; 
plot(ax1, (vol_ICV_0 *100) , 'LineWidth',3, 'color', [0, 0.4470, 0.7410]);
plot(ax1, (vol_ICA_0 *100) , 'LineWidth',3, 'color', [0.6350, 0.0780, 0.1840]);
plot(ax1, meshVolumeFraction*100 , 'LineWidth',3, 'color', [0.4940, 0.1840, 0.5560]);
xline(30,'--k'); xline(70,'--k');
text(7,5,'layer V/VI','fontsize',12); text(45,5,'layer IV','fontsize',12);text(75,5,'layer I/II/III','fontsize',12);
hold off;
legend ('Total vasculature', 'intracortical veins', 'intracortical arteries', 'laminar netwrok');
xlabel('WM                    Relative Cortical Depth (%)                    CSF',  'FontSize',14); ylabel('Baseline CBV(%)', 'FontSize',14)                                                                         
ylim([0 7]); xlim([0 100]); title ('Baseline Blood Volume of the Total Vasculature','FontSize',12);
saveas(gcf, fullfile(VR.folder_name, 'baselineCBV'))
%%
ICAs_arterioles = mean(vol_ICA_0+ 0.43*meshVolumeFraction)*100;
ICVs_ven_cap = mean(vol_ICV_0+ 0.57*meshVolumeFraction)*100;
%% Show message in command window
fprintf('\n%s\n%s\n%s\n',VR.separatingAsterisks,'Vascular model built.', VR.separatingAsterisks)
close all

end