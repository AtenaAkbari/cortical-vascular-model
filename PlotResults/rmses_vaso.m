function rmses_vaso (NVC, VR)
figure(3)

min20RMSE_VASO = 1.2*VR.min_rmse_vaso;
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
d = ((squeeze(data(:,:,v(1),f(1)))./VR.min_rmse_vaso)-1)*100;           %position of min RMSE when ICAs are constant
contourf(X*100,Y*100,d,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,d,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 1;
view(0,90)
xlabel('\DeltaCBV_{venules}', 'fontsize',14,  'FontWeight', 'bold'); ylabel('\DeltaCBV_{arterioles & capillaries}','fontsize',14, 'FontWeight', 'bold'); title ('VASO RMSEs; arterioles & capillaries vs venules','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso1'))
%%
figure(4)
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
r = ((squeeze(data(:,n(1),:,f(1)))./VR.min_rmse_vaso)-1)*100;
contourf(X*100,Y*100,r,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,r,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 2;
view(0,90)
xlabel('\DeltaCBV_{ICAs}', 'fontsize',14,'FontWeight', 'bold'); ylabel('\DeltaCBV_{arterioles & capillaries}','fontsize',14,'FontWeight', 'bold'); title ('VASO RMSEs; arterioles & capillaries vs ICAs','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso2'))
%%
figure(5)
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
b = ((squeeze(data(:,n(1),v(1),:))./VR.min_rmse_vaso)-1)*100;           %position of min RMSE when venules are constant
contourf(X*100,Y*100,b,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,b,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 2;
view(0,90)
xlabel('\DeltaCBV_{ICVs}', 'fontsize',14,'FontWeight', 'bold'); ylabel('\DeltaCBV_{arterioles & capillaries}','fontsize',14,'FontWeight', 'bold'); title ('VASO RMSEs; arterioles & capillaries vs ICVs','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso3'))
%% 
figure(6)
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
s = ((squeeze(data(l(1),:,:,f(1)))./VR.min_rmse_vaso)-1)*100;
contourf(X*100,Y*100,s,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,s,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 2;
view(0,90)
xlabel('\DeltaCBV_{venules}', 'fontsize',14,'FontWeight', 'bold'); ylabel('\DeltaCBV_{ICAs}','fontsize',14,'FontWeight', 'bold'); title ('VASO RMSEs; venules vs ICAs','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso4'))
%%
figure(7)
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
q = ((squeeze(data(l(1),:,v(1),:))./VR.min_rmse_vaso)-1)*100;
contourf(X*100,Y*100,q,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,q,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 2;
view(0,90)
xlabel('\DeltaCBV_{venules}', 'fontsize',14,'FontWeight', 'bold'); ylabel('\DeltaCBV_{ICVs}','fontsize',14,'FontWeight', 'bold'); title ('VASO RMSEs; venules vs ICVs','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso5'))
%%
figure(8)
g = length(NVC.delta_CBV_range);
data = reshape(VR.RMSE_VASO, [g g g g]); 
[l,n,v,f] = ind2sub(size(data),find(data == min(VR.RMSE_VASO)));  
[X, Y] = meshgrid(NVC.cbv_values*1.5, NVC.cbv_values*1.5);           % you can multiply this with 1.5 to show the whole range    
a = ((squeeze(data(l(1),n(1),:,:))./VR.min_rmse_vaso)-1)*100;           %position of min RMSE when arterioles and venules are constant
contourf(X*100,Y*100,a,1000,'LineStyle','none');
hold on;
[m,h]=contour(X*100,Y*100,a,[min20RMSE_VASO min20RMSE_VASO].*100, 'w--'); h.LineWidth = 2;
view(0,90)
xlabel('\DeltaCBV_{ICVs}', 'fontsize',14,'FontWeight', 'bold'); ylabel('\DeltaCBV_{ICAs}','fontsize',14,'FontWeight', 'bold'); title ('VASO RMSEs; ICAs vs ICVs','fontsize',14)
colorbar; colormap parula;
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'rmse_vaso6'))
end