% PLOT VASO PROFILES
% Code to plot the results obtained from breakDownBoldSignal

%%
function [VR] = plot_VASOprofiles (MR, VR, NVC)
% vasc = 1; % 1: Total, 2: VR.microVasc, 3: macroVasc

curFolder = fullfile( VR.folder_name, 'simulation_results' );

vasc = 1;             % Select all vasculature in the voxel
seq = 1;
showConvolved = 1;
for field = 1: length(MR.B0)
[nulled_VASO, BC_VASO] = convolveProfiles(VR,NVC, vasc, seq, field,showConvolved,'VASO');        
VASO_nulled = nulled_VASO *100;
VASO_BC     = BC_VASO * 100;

%Calculate RMSEs  
VR.RMSE_VASO = zeros (1,length(NVC.delta_CBV_range)^4);
for l = 1: (length(NVC.delta_CBV_range)^4)
    VR.RMSE_VASO (:,l) = sqrt(mean((abs(VR.VASO_normalized_to_base) - abs(VASO_BC(:,l)')).^2)); 
end
 
[row_min ,column_min] = min (VR.RMSE_VASO);                                              % Minimum RMSE.
VR.min_rmse_vaso = min (VR.RMSE_VASO);
VR.deltaCBV_bestFit = VR.d_CBV(:,column_min);                                            % delta CBV corresponding to the best fit (i.e the one with the minmum RMSE).
[row_20 ,column_20] = find(VR.RMSE_VASO <= (row_min+(.2*row_min)));                      % Find RMSEs that are 20% higher than the min RMSE, aka the 20% regime.
VASO_BC_20Regime = VASO_BC(:,column_20);                                                 % VASO profiles correspond to the 20% regime. 
[row_min_20Regime ,column_min_20Regime] = min(VASO_BC_20Regime, [], 2);                  % Find the min and max whithin the 20% regime. 
[row_max_20Regime ,column_max_20Regime] = max(VASO_BC_20Regime, [], 2);


% [row_20 ,column_20] = find(RMSE_VASO < (row_min+(.2*row_min)));                        
selected_cbv = VR.d_CBV(:,column_20);                                                    % the deltaCBV of all vascular compartments correspond to the 20% regime. 
min(selected_cbv(1,:)) ;                                                                 % the min and max of the areterioles and capillaries deltaCBV whithin the 20% regime.
max(selected_cbv(1,:));     

min(selected_cbv(4,:)) ;                                                                % the min and max of the venules deltaCBV whithin the 20% regime.
max(selected_cbv(4,:));     

min(selected_cbv(6,:)) ;                                                                % the min and max of the ICAs deltaCBV whithin the 20% regime. 
max(selected_cbv(6,:));     

min(selected_cbv(7,:)) ;                                                               % the min and max of the ICVs deltaCBV whithin the 20% regime. 
max(selected_cbv(7,:));     

%% Plot best fit and the 20% regime
figure (1)
ax1 = linspace(10,90,8);
curve1 = row_min_20Regime'; curve2 = row_max_20Regime';
plot(ax1, abs(curve1)); hold on; plot(ax1, abs(curve2));
x2 = [ax1, fliplr(ax1)];
inBetween = [abs(curve1), fliplr(abs(curve2))];
fill(x2, inBetween,[0.6 .8 1]); 
plot(ax1, abs(curve1), 'color','w'); hold on; plot(ax1, abs(curve2), 'color','w');
errorbar(ax1,abs(VR.VASO_normalized_to_base), VR.err_bar_VASO,'LineWidth',2,'color', 'b'); hold on;
plot(ax1, abs(VASO_BC(:,column_min)),'LineWidth',1, 'color', 'k', 'linestyle', '-.', 'linewidth', 2)
ylim([0 2]);
ylabel ('Signal Change (%)', 'FontSize',14); xlim([0 100]);
xlabel('WM                Relative Cortical Depth (%)                CSF',  'FontSize',14)
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'bestFit_VASO'))
title('VASO', 'fontweight', 'bold', 'fontsize', 16)

%% Plot uncorrected and corrected VASO 
figure (2)
ax1 = linspace(10,90,8);
plot(ax1,abs(VASO_nulled(:,column_min)),'color', [.8 0 .6], 'linewidth', 3); hold on
plot(ax1,abs(VASO_BC(:,column_min)), 'color', [.2 .6 .2], 'linewidth', 3); 
ylim([0 8])
ylabel ('Signal Change (%)', 'FontSize',14); xlim([0 100]) ; 
xlabel('WM              Relative Cortical Depth (%)               CSF',  'FontSize',14)
legend('Uncorrected VASO','BOLD-corrected VASO', 'location', 'northwest'); legend box off
set(gca,'XTick',[0 20 40 60 80 100])
saveas(gcf, fullfile(VR.folder_name, 'corrected_Uncorrected_VASO'))

%% plot VASO rmses 
rmses_vaso (NVC, VR);
%% 
fprintf('%s\n%s\n%s\n%s\n',VR.separatingAsterisks, 'deltaCBV (%) in:','1)macro- and micro-arterioles:',num2str(VR.d_CBV(1,column_min)*100),...
    '2)capillaries:',num2str(VR.d_CBV(3,column_min)*100),'3)macro- and micro-venules:',num2str(VR.d_CBV(4,column_min)*100),...
    '4)ICAs:',num2str(VR.d_CBV(6,column_min)*100),'5)ICVs:',num2str(VR.d_CBV(7,column_min)*100))

end

