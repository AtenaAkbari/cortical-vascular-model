
% PLOT_BOLDPROFILES
%
% Irati Markuerkiaga, 27.01.2014
%
% Code to plot the results obtained from breakDownBoldSignal

%%
function [] = plot_BOLDprofiles (MR, VR, NVC)
% vasc = 1; % 1: Total, 2: VR.microVasc, 3: macroVasc
close all;
curFolder = fullfile( VR.folder_name, 'simulation_results' );
% IV/EV CONTRIBUTIONS
vasc = 1; % Select all vasculature in the voxel
seq = 1; %GE
showConvolved = 1;
for field = 1: length(MR.B0)
    [~, ~, inC_BOLD, exC_BOLD,totalC_BOLD] = convolveProfiles(VR, NVC, vasc, seq, field,showConvolved,'BOLD');
    BOLD_ex = exC_BOLD*100;
    BOLD_tot = totalC_BOLD *100;
    BOLD_in = inC_BOLD*100;
        
% Calculate RMSEs
n = length(NVC.ven_oxy_base);
RMSE_BOLD = zeros (1,n^2);
for g = 1: n^2
RMSE_BOLD (:,g) = sqrt(mean((VR.BOLD_normalized_to_base - BOLD_tot(:,g)').^2)); 
end

[row_min ,column_min] = min (RMSE_BOLD);                                      % minimum RMSE
VR.OXY(:,:,column_min);
[row_20 ,column_20] = find(RMSE_BOLD<=(row_min+.2*row_min));                  % Find RMSEs that are 20% higher than the minimum RMSE, aka 20% regime. 
BOLDtot_20Regime = BOLD_tot(:,column_20);                                     % BOLD profiles correspond to the 20% regime. 
[row_min_20Regime ,column_min_20Regime] = min(BOLDtot_20Regime, [], 2);       % Find the minimum and maximum in the 20% regime. 
[row_max_20Regime ,column_max_20Regime] = max(BOLDtot_20Regime, [], 2);

selected_oxy = VR.OXY(:,:,column_20);                                         % Oxygen saturation values correspond to the profiles in the 20% regime. 
min(min(selected_oxy(:,4,:)));                                                % The min and max of the oxygen saturation at baseline whitin the 20% regime. 
max(max(selected_oxy(:,4,:)));

min(min(selected_oxy(:,5,:)));                                                % The min and max of the oxygen saturation at activation whithin the 20% regime. 
max(max(selected_oxy(:,5,:)));


%% Plot the BOLD Intra- and extra-vascular signals
figure (9)
ax1 = linspace(10,90,8);
plot(ax1,BOLD_in(:,column_min),'color',[.6 0 .2] , 'linewidth', 3); hold on
plot(ax1,BOLD_ex(:,column_min), 'color', [1 .6 .2], 'linewidth', 3);
legend ('BOLD IV', 'BOLD EV', 'location', 'northwest'); legend boxoff
ylabel ('Signal Change (%)', 'FontSize',12); xlabel('WM                    Relative Cortical Depth                    CSF',  'FontSize',14)
ylim([0 8]); xlim([0 100])
set(gca,'XTick',[0 20 40 60 80 100]);
saveas(gcf, fullfile(VR.folder_name, 'IV_EV_BOLD'))

%% Plot best fit and 20% regime 
figure (10)
ax1 = linspace(10,90,8);
curve1 = row_min_20Regime'; curve2 = row_max_20Regime';
plot(ax1, curve1, 'color','w'); hold on; plot(ax1, curve2, 'color','w');
x2 = [ax1, fliplr(ax1)];
inBetween = [curve1, fliplr(curve2)];
fill(x2, inBetween,[1 .7 .7]); 
hold on
plot(ax1, curve1, 'color','w'); hold on; plot(ax1, curve2, 'color','w');
errorbar(ax1, VR.BOLD_normalized_to_base, VR.err_bar_BOLD,'LineWidth',2,'color', [.8 0 .2]);
plot(ax1,BOLD_tot(:,column_min),'LineWidth',1, 'color', 'k', 'linestyle', '-.', 'linewidth', 2); hold on
ylim([0 10]); xlim([0 100]);
ylabel ('Signal Change (%)', 'FontSize',12); xlabel('WM                    Relative Cortical Depth                    CSF',  'FontSize',14)
set(gca,'XTick',[0 20 40 60 80 100]); title('BOLD', 'fontweight', 'bold', 'fontsize',16)
saveas(gcf, fullfile(VR.folder_name, 'bestFit_BOLD'))
%% Plot RMSEs
figure (11)
oxy_base = .6:.01:.75;
oxy_act = .75:.01:.9;
min20RMSE_BOLD = 1.2*min(RMSE_BOLD);
t = (((reshape(RMSE_BOLD,[],16))./min(RMSE_BOLD))-1)*100;
[X,Y] = meshgrid(oxy_act*100,oxy_base*100);
v = [min20RMSE_BOLD ,min20RMSE_BOLD]*100; 
contourf(oxy_base*100,oxy_act*100,t,1000,'LineStyle','none');
hold on;
[m,h]=contour(oxy_base*100,oxy_act*100,t,v,'w--'); h.LineWidth = 2;
view(0,90)
zlabel('RMSE','FontWeight', 'bold', 'FontSize',14); xlabel('Y_{activation}', 'FontSize',14,'FontWeight', 'bold'); ylabel('Y_{baseline}', 'FontSize',14,'FontWeight', 'bold'); title ('BOLD RMSEs','fontsize',14)
shading interp
colorbar; colormap parula
view (0, 90);
colormap hot;
saveas(gcf, fullfile(VR.folder_name, 'RMSE_BOLD'))
end
%% Show message in command window
close all;
fprintf('%s\n%s\n%s\n', VR.separatingAsterisks,'Oxygenation at baseline (%) in:','macro- and micro-venules:',num2str((VR.OXY(4,4,column_min)*100)),'ICVs:',num2str((VR.OXY(6,4,column_min)*100)),VR.separatingAsterisks,...
    'Oxygenation at activation (%) in:', 'macro- and micro-venules:',num2str((VR.OXY(4,5,column_min)*100)),'ICVs:',num2str((VR.OXY(6,5,column_min)*100)),VR.separatingAsterisks, 'total time: ', toc)

