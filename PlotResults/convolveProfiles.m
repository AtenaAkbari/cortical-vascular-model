function [nulled_VASO,BC_VASO, inC_BOLD, exC_BOLD,totalC_BOLD] = convolveProfiles(VR, NVC, vasc, seq, field, showConvolved, modality)
if showConvolved
if (strcmp(modality, 'VASO'))
    n = length(NVC.delta_CBV_range);
    nulled_VASO = zeros(8, n^4);
    BC_VASO     = zeros(8, n^4);
    for h = 1: n^4
    nulled_VASO (:,h) = conv( squeeze(VR.S_nulled_VASO(vasc,seq,:,field,h)), VR.convKernel, 'valid');
    BC_VASO (:,h)     = conv(squeeze(VR.S_VASO(vasc,seq,:,field,h))        , VR.convKernel, 'valid');
    end

elseif (strcmp(modality, 'BOLD'))
    inC_BOLD = zeros(8, 256);
    exC_BOLD  = zeros(8, 256);
    totalC_BOLD = zeros(8, 256);
    for h = 1: 256
    inC_BOLD (:,h) = conv(squeeze(VR.S_BOLD_in(vasc,seq,:,field,h)), VR.convKernel, 'valid');
    exC_BOLD(:,h) = conv( squeeze(VR.S_BOLD_ex(vasc,seq,:,field,h)), VR.convKernel, 'valid');
    totalC_BOLD(:,h) = conv(squeeze(VR.S_BOLD(vasc,seq,:,field,h)),VR.convKernel, 'valid');
    nulled_VASO = [];
    BC_VASO = [];
    end

end
else
    
  if (strcmp(modality, 'VASO'))
    n = length(NVC.delta_CBV_range);
    nulled_VASO = zeros(10, n^4);
    BC_VASO     = zeros(10, n^4);
    for h = 1: n^4
    nulled_VASO (:,h) = ( squeeze(VR.S_nulled_VASO(vasc,seq,:,field,h)));
    BC_VASO (:,h)     = (squeeze(VR.S_VASO(vasc,seq,:,field,h)));
    end

elseif (strcmp(modality, 'BOLD'))
    inC_BOLD = zeros(10, 256);
    exC_BOLD  = zeros(10, 256);
    totalC_BOLD = zeros(10, 256);
    for h = 1: 256
    inC_BOLD (:,h) = (squeeze(VR.S_BOLD_in(vasc,seq,:,field,h)));
    exC_BOLD(:,h) = ( squeeze(VR.S_BOLD_ex(vasc,seq,:,field,h)));
    totalC_BOLD(:,h) = (squeeze(VR.S_BOLD(vasc,seq,:,field,h)));
    nulled_VASO = [];
    BC_VASO = [];
    end

end  
    
    
    
end
