# laminarVascularModel
 
**Purpose:**

The cortical vascular model (Markuerkiaga et al., 2016) divides tha human vasculature into two groups of the laminar netwrok (i.e. arterioles, capillaries, and venules) and intracortical veins. The model then utilizes the "integrative BOLD signal model" (Uludag et al., 2009) to simulate the micro-, macro-vascular, and total BOLD signal at each cortical depth in the human primary visual cortex. Since VASO is an arterial contrast, we added the intracortical arteries to the model to simulate the VASO contrast in addition to BOLD. 
The model can be easily adapted for other brain regions given the known layer thicknesses and baseline CBV. Arbitrary number of depths can be defined across the modelled area (here we defined10 depths for V1). 

**Getting started:**

The code is implemented in MATLAB. The general structure of the code is as follows:
- initSimulSettings.m
    - All the model parameters (e.g. vasculature, neurovascular coupling, and MR imaging parameters) are defined here. 
- After initializing the parameters, the vascular model is built using “buildVascularModel_1xG4G3_2xA4A3”. In this function, the “getMeshVolPerLayer.m” is called that interpolates the number of baseline CBV  data points (7 data points from Weber et al., 2008) to the number of simulated voxels (10 voxels in our study).
- Then “SignalAcrossLayers.m” first calculates the VASO signal change at each layer, finds the CBV change of each vascular compartments from the best-fit VASO simulation, and then calculate the BOLD signal change at each layer. 
- The intra-, extra-vascular, and total signal change are computed in “breakDownSignal.m” 
- Finally, the convolution kernel is applied and the resulting profiles are plotted using “plot_VASOprofiles.m” and “plot_BOLDprofiles.m. 

In "vasoSignal" branch, you can see the earlier version of the VASO simulation (without BOLD) that was presented at ISMRM 2020. 
The original version of the code developed by Markuerkiaga et al., (2016) is available in “origin” branch. 


**Contact/support:**

We would be happy to receive your feedback to improve the code. Please contact akbariatena86@gmail.com if you have any question/feedback. 
